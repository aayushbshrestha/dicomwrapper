using DICOMOpv.Models;
using Topcon.DicomWrapper;
using Xunit;

namespace DicomLibraryTests
{
    public class DicomLibraryTester
    {
        [Theory]
        [InlineData("./Resources/HFA_Central24-2_Left_11.dcm")]
        public void TestDataExtraction(string path)
        {
            DicomOpv test = new DicomOpv(path);
            ReportType value = test.GetDicomReportType();
            Assert.NotNull(test.ExtractedText);
        }

        [Theory]
        [InlineData("./Resources/DEMO_FGW_07_19440901_20050923_0824_SFA_OD_1.2.276.0.75.2.5.80.23.3.131024143242131.52229801866.698970067.dcm")]
        public void TestGazeTracker(string path)
        {
            DicomOpv dicomObj = new DicomOpv(path);
            var Report = dicomObj.GetDicomReportType();
            Assert.True(System.IO.File.Exists("DEMO_FGW_07.jpg"));
        }
    }
}