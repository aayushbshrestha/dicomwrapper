﻿using Dicom;
using DICOMOpv.Export;
using DICOMOpv.Models;
using DICOMOpv.Processors;
using DICOMOpv.Serializer;
using DICOMOpv.Util;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace Topcon.DicomWrapper
{
    public class DicomOpv
    {
        #region params

        public uint AccessionNumber;
        public DateTime? AcquisitionDateTime;
        public string ConversionType;
        public string DeviceSerialNumber;
        public string DocumentTitle;
        public string EncapsulatedDocContentType;
        public DicomElement EncapsulatedDocument;
        public Gender PatientSex;
        public DateTime? InstanceCreationDateTime;
        public string InstitutionName;
        public uint InstanceNumber;
        public string InstitutionAddress;
        public string IssuerOfPatientID;
        public Laterality Laterality;
        public string Manufacturer;
        public string ManufacturerModelName;
        public DateTime? PatientDOB;
        public string PatientID;
        public DicomPersonName PatientName;
        public DicomPersonName ReferringPhysicianName;
        public string SOPClassUID;
        public string SOPInstanceUID;
        public string SpecificCharacterSet;
        public string StationName;
        public DateTime? StudyDateTime;
        public string StudyDescription;
        public string SoftwareVersions;
        public string StudyInstanceUID;
        public string SeriesInstanceUID;
        public string StudyID;
        public uint SeriesNumber;
        public string ExtractedText;

        public ulong FileMetaInformationGroupLength;
        public string MediaStorageSOPClassUID;
        public string TransferSyntaxUID;
        public string ImplementationClassUID;
        public DicomSequence ProcedureCodeSequence;
        public DicomSequence PerformedProtocolCodeSequence;
        public DiscreteDataItem TestType;
        public DicomSequence FixationSequence;
        public DiscreteDataItem FixationLosses;
        public DicomSequence VisualFieldCatchTrialSequence;
        public float VisualFieldTestDuration; // TestDuration;
        public DiscreteDataItem Stimulus;
        public DicomSequence BackgroundIlluminationColorCodeSequence;
        public float BackgroundLuminance;
        public DiscreteDataItem Strategy;
        public DicomSequence OphthalmicPatientClinicalInformationRightEyeSequence;
        public DiscreteDataItem PupilDiameter;
        public DiscreteDataItem VisualAcuity;
        public DiscreteDataItem Rx;
        public string PerformedProcedureStepStartDate; //TestDate;
        public string PerformedProcedureStepStartTime;// TestTime;
        public DicomSequence ResultsNormalsSequence;
        public float FovealSensitivity;  //Fovea
        public DicomSequence VisualFieldTestPointSequence;

        public ReportType ReportType;
        public Language DetectedLanguage;

        private static readonly string[] SFA_Values = new string[] { "Single Field Analysis", "Prueba de umbral" };

        public ValueMatrix TotalDeviationValueMatrix;
        public ValueMatrix PatternDeviationValueMatrix;
        public ValueMatrix SensitivityValueMatrix;

        public SymbolMatrix TotalDeviationSymbolMatrix;
        public SymbolMatrix PatternDeviationSymbolMatrix;

        #region SerializationItems

        public static string OriginalFilePath;
        public static FileType SourceType;
        public static string SerializerMachineType;
        public static IList<TextItem> TextTagValues;
        public static IList<MatrixItem> MatrixItems;
        public static IList<ImageItem> ImageItems;

        #endregion SerializationItems

        private static byte[] ReportImageBytes;

        #endregion params

        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Main

        private DicomOpv()
        {
        }

        /// <summary>
        /// Single entry point for library
        /// </summary>
        /// <param name="file">Path to file to be processed</param>
        /// <param name="language">Language present in report</param>
        public DicomOpv(string file, string language = null)
        {
            string ext = Path.GetExtension(file).ToLower();
            OriginalFilePath = file;
            if (!string.IsNullOrEmpty(language))
            {
                AssignLanguage(language);
            }
            LoadLoggerConfigs();
            if (ext == ".dcm")
            {
                SourceType = FileType.DICOM;
                //ConstructObject(file);
                ConstructForExport(file);
            }
            else if (ext == ".pdf")
            {
                SourceType = FileType.PDF;
                DicomFromPDF(file);
            }
            else if (ext == ".jpg")
            {
                SourceType = FileType.JPG;
                DicomFromJPG(file);
            }
            else
            {
                SourceType = FileType.Unkown;
                log.Error($"Could not process {file}, {SourceType} is not supported.");
                return;
            }
        }

        public void Extract()
        {
            if (!string.IsNullOrEmpty(SourceType.ToString()))
            {
                if (SourceType == FileType.DICOM)
                {
                    var report = GetDicomReportType();
                    if (report.isValid)
                        ProcessImages(report);
                    else
                        log.Error("Appropriate Report Type could not be identified hence extraction will be stopped.");
                }
                else if (SourceType == FileType.PDF)
                {
                    var report = GetDicomReportType(OriginalFilePath);
                    if (report.isValid)
                        ProcessImages(report, OriginalFilePath);
                    else
                        log.Error("Appropriate Report Type could not be identified hence extraction will be stopped.");
                }
                else if (SourceType == FileType.JPG)
                {
                    var report = GetDicomReportType(OriginalFilePath, true);
                    if (report.isValid)
                        ProcessImages(report, OriginalFilePath);
                    else
                        log.Error("Appropriate Report Type could not be identified hence extraction will be stopped.");
                }
            }
        }

        private void DicomFromPDF(string filePath)
        {
            log.Info("Since no dicom tags are present to check, all image and text components will be extracted.");
        }

        private void DicomFromJPG(string filePath)
        {
            log.Info("Since no dicom tags are present to check, all image and text components will be extracted.");
        }

        /// <summary>
        /// Constructing a smaller data set for extracting and exporting the report from Dicom input
        /// </summary>
        /// <param name="file"></param>
        private void ConstructForExport(string file)
        {
            try
            {
                log.Info($"Processing: {file}");
                DicomFile dicomFile = DicomFile.Open(file);
                DicomDataset dataset = dicomFile.Dataset;
                EncapsulatedDocument = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.EncapsulatedDocument));
                Laterality = DicomProcessor.GetLateralityFromDicomItem((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Laterality));
                EncapsulatedDocContentType = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.MIMETypeOfEncapsulatedDocument)).Get<string>();
                ManufacturerModelName = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ManufacturerModelName)).Get<string>();
            }
            catch (Exception ex)
            {
                log.Error("Could not construct dicom object elements required for extraction. Exception : " + ex.Message);
            }
        }

        /// <summary>
        /// Constructs full set of dicom values
        /// </summary>
        /// <param name="file"></param>
        public void ConstructObject(string file)
        {
            try
            {
                LoadLoggerConfigs();
                log.Info($"Processing: {file}");
                DicomDataset dataset = DicomFile.Open(file).Dataset;
                if (!DicomProcessor.CheckModality((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Modality)))
                {
                    log.Info("The dicom file we are trying to process is not of DICOM Opv format");
                    return;
                }

                EncapsulatedDocument = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.EncapsulatedDocument));
                Laterality = DicomProcessor.GetLateralityFromDicomItem((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Laterality));
                EncapsulatedDocContentType = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.MIMETypeOfEncapsulatedDocument)).Get<string>();
                PatientName = new DicomPersonName(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientName)).Tag, ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientName)).Get<string>(0));
                InstanceCreationDateTime = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstanceCreationDate))?.Get<string>(),
                                                ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstanceCreationTime))?.Get<string>());
                AcquisitionDateTime = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.AcquisitionDateTime)));
                StudyDateTime = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyDate)).Get<string>(), ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyTime)).Get<string>());
                PatientSex = DicomProcessor.GetPatientGender((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientSex));
                PatientDOB = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientBirthDate))?.Get<string>());
                PatientID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientID)).Get<string>();
                SpecificCharacterSet = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SpecificCharacterSet)).Get<string>();
                SOPClassUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SOPClassUID)).Get<string>();
                SOPInstanceUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SOPInstanceUID)).Get<string>();
                uint.TryParse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.AccessionNumber)).Get<string>(), out AccessionNumber);
                ConversionType = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ConversionType)).Get<string>();
                Manufacturer = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Manufacturer)).Get<string>();

                InstitutionName = DicomProcessor.GetStringValueForDicomElement(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstitutionName)));

                InstitutionAddress = DicomProcessor.GetStringValueForDicomElement(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstitutionAddress)));
                ReferringPhysicianName = DicomProcessor.GetPersonName(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ReferringPhysicianName)));

                StationName = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StationName)).Get<string>();
                StudyDescription = DicomProcessor.GetStringValueForDicomElement((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyDescription));
                ManufacturerModelName = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ManufacturerModelName)).Get<string>();
                IssuerOfPatientID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.IssuerOfPatientID)).Get<string>();
                DeviceSerialNumber = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.DeviceSerialNumber)).Get<string>();
                SoftwareVersions = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SoftwareVersions)).Get<string>();
                StudyInstanceUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyInstanceUID)).Get<string>();
                SeriesInstanceUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SeriesInstanceUID)).Get<string>();
                StudyID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyID)).Get<string>();
                uint.TryParse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SeriesNumber)).Get<string>(), out SeriesNumber);
                uint.TryParse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstanceNumber)).Get<string>(), out InstanceNumber);
                DocumentTitle = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.DocumentTitle)).Get<string>();
                TransferSyntaxUID = dataset.GetDicomItem<DicomItem>(DicomTag.TransferSyntaxUID) == null ? "" : ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.TransferSyntaxUID)).Get<string>();
                FileMetaInformationGroupLength = (DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.FileMetaInformationGroupLength) == null ? 0 : ulong.Parse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.FileMetaInformationGroupLength)).Get<string>());
                MediaStorageSOPClassUID = (DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.MediaStorageSOPClassUID) == null ? "" : ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.MediaStorageSOPClassUID)).Get<string>();

                OpvHelper opvHelper = new OpvHelper();
                opvHelper.PopulateDiscreteNodesAndVisualTestPointSequence();

                /* Start non-matrix discrete elements (Appx 18 nodes)  */
                PerformedProtocolCodeSequence = opvHelper.GetPerformedProtocolCodeSequence();
                ProcedureCodeSequence = opvHelper.ProcCodeSequence();
                FixationSequence = opvHelper.GetFixationSequence();
                VisualFieldCatchTrialSequence = opvHelper.GetVisualFieldCatchTrialSequence();
                VisualFieldTestDuration = opvHelper.GetTestDuration();
                BackgroundIlluminationColorCodeSequence = opvHelper.GetBackgroundIlluminationColorCodeSequence();
                BackgroundLuminance = opvHelper.GetBackgroundLuminance();
                OphthalmicPatientClinicalInformationRightEyeSequence = opvHelper.GetOphthalmicPatientClinicalInformationRightEyeSequence();
                PerformedProcedureStepStartDate = opvHelper.GetTestDate();
                PerformedProcedureStepStartTime = opvHelper.GetTestTime();
                ResultsNormalsSequence = opvHelper.GetResultsNormalsSequence();
                FovealSensitivity = opvHelper.GetFovea();
                /* End non-matrix discrete elements (Appx 18 nodes)  */

                /* Start matrix elements -5 matrix node values   */
                List<OpvHelper.VisualFieldTestPoint> visualFieldTestPoints = opvHelper.VFTPoints;
                VisualFieldTestPointSequence = opvHelper.GetVisualFieldTestPointSequence(visualFieldTestPoints);
                /* End matrix elements    */
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        public static void SetMachineTypeSerializer(string value)
        {
            SerializerMachineType = value;
        }

        #region XML export

        public string ExportReportXML(string outputFolder = null)
        {
            Report report = new Report()
            {
                MachineType = new MachineTypeItem(SerializerMachineType),
                FileType = new ReportSource(SourceType.ToString()),
                TextItems = (List<TextItem>)TextTagValues,
                MatrixItems = (List<MatrixItem>)MatrixItems,
                ImageItems = (List<ImageItem>)ImageItems
            };
            return ExportHandler.Export(report, OriginalFilePath, outputFolder);
        }

        #endregion XML export

        public ReportType GetDicomReportType()
        {
            if (EncapsulatedDocument != null)
            {
                var extractedValues = GetEmbededPdfAsText(EncapsulatedDocument);
                ExtractedText = extractedValues.Item1;
                ReportImageBytes = extractedValues.Item2;
            }
            if (!string.IsNullOrEmpty(ExtractedText))
            {
                var report = ReportTypeProcessor.IdentifyReportType(ExtractedText, ManufacturerModelName, DetectedLanguage);
                TextTagValues = TextProcessor.LoadTextTags(ExtractedText, report);
                log.Info($"Report type identified to be {report.ThresholdType} produced from {report.DeviceType.ToString()} device. Applying template : {report.TemplateType}.");
                return report;
            }
            return null; // if text was not loaded we can't identify report TO DO: This needs to be tested with a lot more data
        }

        public ReportType GetDicomReportType(string path, bool isImage = false)
        {
            if (!string.IsNullOrEmpty(path))
            {
                //conversion needed for pdfs
                if (!isImage)
                {
                    var extractedValues = GetEmbededPdfAsText(File.ReadAllBytes(path));
                    ExtractedText = extractedValues.Item1;
                    ReportImageBytes = extractedValues.Item2;
                }
                else
                {
                    var bytes = File.ReadAllBytes(path);
                    ExtractedText = GetImagePdfAsText(bytes);
                    ReportImageBytes = bytes;
                }
            }
            else
            {
                log.Error("No path found to process..");
                return null;
            }
            if (!string.IsNullOrEmpty(ExtractedText))
            {
                var report = ReportTypeProcessor.IdentifyReportType(ExtractedText, ManufacturerModelName, DetectedLanguage);
                var value = TextProcessor.LoadTextTags(ExtractedText, report, isImage);
                TextTagValues = value.Item1;
                Laterality = value.Item2;
                log.Info($"Report type identified to be {report.ThresholdType.ToString()} produced from {report.DeviceType.ToString()} device. Applying template : {report.TemplateType.ToString()}.");
                return report;
            }
            return null; // if text was not loaded we can't identify report TO DO: This needs to be tested with a lot more data
        }

        private void AssignLanguage(string language)
        {
            switch (language)
            {
                case "ES":
                    DetectedLanguage = Language.ES;
                    break;

                case "JP":
                    DetectedLanguage = Language.JP;
                    break;

                default:
                    DetectedLanguage = Language.EN;
                    break;
            }
            TesseractSingleton.Instance.SetLanguage(DetectedLanguage);
        }

        /// <summary>
        /// For Dicom file
        /// </summary>
        /// <param name="report"></param>
        public void ProcessImages(ReportType report)
        {
            if (EncapsulatedDocument != null && EncapsulatedDocContentType.Contains("pdf"))
            {
                string path;
                if (ReportImageBytes == null) // if the image conversion was not done previously
                {
                    path = ImageProcessor.GenerateHelperImages(EncapsulatedDocument.Buffer.Data, report);
                }
                else
                {
                    path = ImageProcessor.GenerateHelperImages(ReportImageBytes, report, true);
                }
                var data = ImageProcessor.GenerateAccesories(path, report, Laterality);
                AssignValueMatrices(data.Item2);
                AssignSymbolMatrices(data.Item3);
                //generate serialization object here.
                GetSerializableMatrices(report);
                GetSerializableImages(data.Item4);
                //since path to data is also returned here potentially delete them after generating serializable objects.
                CleanUpRoot(data.Item1);
            }
        }

        public void ProcessImages(ReportType report, string source)
        {
            try
            {
                if (!string.IsNullOrEmpty(source))
                {
                    var path = ImageProcessor.GenerateHelperImages(File.ReadAllBytes(source), report);
                    var data = ImageProcessor.GenerateAccesories(path, report, Laterality);
                    AssignValueMatrices(data.Item2);
                    AssignSymbolMatrices(data.Item3);
                    //generate serialization object here.
                    GetSerializableMatrices(report);
                    GetSerializableImages(data.Item4);
                    //since path to data is also returned here potentially delete them after generating serializable objects.
                    CleanUpRoot(data.Item1);
                }
                else
                {
                    log.Error("Source could not be found.");
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error encountered while trying to process images : " + ex.Message);
                return;
            }
        }

        private void CleanUpRoot(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    log.Error($"Root not found to clean up. path : {path}");
                    return;
                }
                Directory.Delete(path, true);
            }
            catch (Exception ex)
            {
                log.Error("Could not clean up root.");
                log.Error(ex.Message);
            }
        }

        private void GetSerializableImages(Dictionary<string, string> images)
        {
            ImageItems = new List<ImageItem>();
            foreach (var item in images)
            {
                ImageItems.Add(new ImageItem(item.Key, item.Value, null));
            }
        }

        private void GetSerializableMatrices(ReportType report)
        {
            MatrixItems = new List<MatrixItem>();
            try
            {
                MatrixItems.Add(MatrixProcessor.GetValueMatrixItems(TotalDeviationValueMatrix, report));
                MatrixItems.Add(MatrixProcessor.GetValueMatrixItems(PatternDeviationValueMatrix, report));
                MatrixItems.Add(MatrixProcessor.GetValueMatrixItems(SensitivityValueMatrix, report));
                MatrixItems.Add(MatrixProcessor.GetSymbolMatrixItems(PatternDeviationSymbolMatrix, report));
                MatrixItems.Add(MatrixProcessor.GetSymbolMatrixItems(TotalDeviationSymbolMatrix, report));
            }
            catch (Exception ex)
            {
                log.Error($"Could not serialize matrix items.\n{ex.Message}\n{ex.StackTrace}");
            }
        }

        private void AssignValueMatrices(List<ValueMatrix> values)
        {
            foreach (var value in values)
            {
                switch (value.type)
                {
                    case MatrixTypes.PatternDeviation:
                        PatternDeviationValueMatrix = value;
                        break;

                    case MatrixTypes.TotalDeviation:
                        TotalDeviationValueMatrix = value;
                        break;

                    case MatrixTypes.Sensitivity:
                        SensitivityValueMatrix = value;
                        break;
                }
            }
        }

        public void AssignSymbolMatrices(List<SymbolMatrix> values)
        {
            foreach (var value in values)
            {
                switch (value.type)
                {
                    case MatrixTypes.PatternDeviation:
                        PatternDeviationSymbolMatrix = value;
                        break;

                    case MatrixTypes.TotalDeviation:
                        TotalDeviationSymbolMatrix = value;
                        break;
                }
            }
        }

        /// <summary>
        /// Main entrypoint for library.
        /// This method constructs a DICOM OPV object and returns it to the invoker
        /// </summary>
        /// <param name="file">path to DICOM file</param>
        /// <returns></returns>
        public static DicomOpv FromFile(string file)
        {
            try
            {
                LoadLoggerConfigs();
                log.Info($"Processing: {file}");
                DicomFile dicomFile = DicomFile.Open(file);
                DicomDataset dataset = dicomFile.Dataset;

                if (!DicomProcessor.CheckModality((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Modality)))
                {
                    log.Info("The dicom file we are trying to process is not of DICOM Opv format");
                    return null;
                }
                return new DicomOpv
                {
                    EncapsulatedDocument = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.EncapsulatedDocument)),
                    Laterality = DicomProcessor.GetLateralityFromDicomItem((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Laterality)),
                    EncapsulatedDocContentType = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.MIMETypeOfEncapsulatedDocument)).Get<string>(),
                    PatientName = new DicomPersonName(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientName)).Tag, ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientName)).Get<string>(0)),
                    InstanceCreationDateTime = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstanceCreationDate))?.Get<string>(),
                                                    ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstanceCreationTime))?.Get<string>()),
                    AcquisitionDateTime = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.AcquisitionDateTime))),
                    StudyDateTime = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyDate)).Get<string>(), ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyTime)).Get<string>()),
                    PatientSex = DicomProcessor.GetPatientGender((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientSex)),
                    PatientDOB = DicomProcessor.DicomDateTimeParser(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientBirthDate))?.Get<string>()),
                    PatientID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.PatientID)).Get<string>(),
                    SpecificCharacterSet = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SpecificCharacterSet)).Get<string>(),
                    SOPClassUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SOPClassUID)).Get<string>(),
                    SOPInstanceUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SOPInstanceUID)).Get<string>(),
                    AccessionNumber = uint.Parse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.AccessionNumber)).Get<string>()),
                    ConversionType = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ConversionType)).Get<string>(),
                    Manufacturer = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.Manufacturer)).Get<string>(),
                    InstitutionName = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstitutionName)).Get<string>(),
                    InstitutionAddress = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstitutionAddress)).Get<string>(),
                    ReferringPhysicianName = new DicomPersonName(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ReferringPhysicianName)).Tag,
                                                    ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ReferringPhysicianName)).Get<string>(0)),
                    StationName = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StationName)).Get<string>(),
                    StudyDescription = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyDescription)).Get<string>(),
                    ManufacturerModelName = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.ManufacturerModelName)).Get<string>(),
                    IssuerOfPatientID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.IssuerOfPatientID)).Get<string>(),
                    DeviceSerialNumber = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.DeviceSerialNumber)).Get<string>(),
                    SoftwareVersions = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SoftwareVersions)).Get<string>(),
                    StudyInstanceUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyInstanceUID)).Get<string>(),
                    SeriesInstanceUID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SeriesInstanceUID)).Get<string>(),
                    StudyID = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.StudyID)).Get<string>(),
                    SeriesNumber = uint.Parse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.SeriesNumber)).Get<string>()),
                    InstanceNumber = uint.Parse(((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.InstanceNumber)).Get<string>()),
                    DocumentTitle = ((DicomElement)dataset.GetDicomItem<DicomItem>(DicomTag.DocumentTitle)).Get<string>()
                };
            }
            catch (Exception ex)
            {
                log.Error($"Error encoutered trying to construct dicom object. {ex.Message}");
                return null;
            }
        }

        #endregion Main

        #region Util

        /// <summary>
        /// Load config for logger
        /// </summary>
        private static void LoadLoggerConfigs()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }

        public static (string, byte[]) GetEmbededPdfAsText(DicomElement pdf)
        {
            try
            {
                if (pdf != null)
                {
                    var bytes = TextProcessor.ReturnImageBytesFromPdf(pdf.Buffer.Data);
                    return (TesseractSingleton.Instance.ProcessPage(bytes), bytes);
                }
                log.Info("Pdf element was null, no text to process.");
                return (null, null);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return (null, null);
            }
        }

        public static (string, byte[]) GetEmbededPdfAsText(byte[] pdf)
        {
            try
            {
                if (pdf != null)
                {
                    var bytes = TextProcessor.ReturnImageBytesFromPdf(pdf);
                    return (TesseractSingleton.Instance.ProcessPage(bytes), bytes);
                }
                log.Info("Pdf element was null, no text to process.");
                return (null, null);
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return (null, null);
            }
        }

        public static string GetImagePdfAsText(byte[] pdf)
        {
            try
            {
                if (pdf != null)
                {
                    return TesseractSingleton.Instance.ProcessPage(pdf);
                }
                log.Info("Pdf element was null, no text to process.");
                return null;
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return null;
            }
        }

        #endregion Util
    }
}