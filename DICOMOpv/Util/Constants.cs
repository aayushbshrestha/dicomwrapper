﻿namespace DICOMOpv.Util
{
    public class Constants
    {
        public static readonly string GAZE = "GazeTracker";
        public static readonly string TOTAL_DEVIATION_VALUE_MATRIX = "TotalDeviationValueMatrix";
        public static readonly string TOTAL_DEVIATION_SYMBOL_MATRIX = "TotalDeviationSymbolMatrix";
        public static readonly string SENSITIVITY_VALUE_MATRIX = "SensitivityValueMatrix";
        public static readonly string PATTERN_DEV_VALUE_MATRIX = "PatternDeviationValueMatrix";
        public static readonly string PATTERN_DEV_SYMBOL_MATRIX = "PatternDeviationSymbolMatrix";
        public static readonly string GREY_SCALE = "GreyscalePlot";
    }
}