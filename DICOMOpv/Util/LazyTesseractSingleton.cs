﻿using System;
using Tesseract;

namespace DICOMOpv.Util
{
    /// <summary>
    /// Lazy implementation of tesseract singleton needs to be tested for potential performance benefits
    /// also seems to be more explicit
    /// instead of relying on an IL generation side-effect, the Lazy<T> makes it more explicit that we're trying to do lazy construction
    /// </summary>
    public sealed class LazyTesseractSingleton
    {
        private static readonly Lazy<LazyTesseractSingleton> lazy = new Lazy<LazyTesseractSingleton>(() => new LazyTesseractSingleton());
        public static LazyTesseractSingleton Instance { get { return lazy.Value; } }

        private LazyTesseractSingleton()
        {
        }

        private static TesseractEngine engine;

        public static TesseractEngine getEngine()
        {
            if (engine != null)
            {
                return engine;
            }
            else
            {
                engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
                engine.SetVariable("user_defined_dpi", "300");
            }
            return engine;
        }

        private void initEngine()
        {
            if (engine != null)
            {
                return;
            }
            else
            {
                engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default);
                engine.SetVariable("user_defined_dpi", "300");
            }
        }

        public string GetText(byte[] data)
        {
            initEngine();
            using (var img = Pix.LoadFromMemory(data))
            {
                using (var page = engine.Process(img, PageSegMode.SingleBlock))
                {
                    return SanitizeString(page.GetText());
                }
            }
        }

        private static string SanitizeString(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            if (input.Contains("\n"))
                input = input.Replace("\n", "");

            if (input.Contains("="))
                input = input.Replace("=", "-");

            if (input.Contains("."))
                input = input.Replace(".", "-");

            if (input.Contains("'"))
                input = input.Replace("'", "-");

            if (input.Contains("‘"))
                input = input.Replace("‘", "");

            return input;
        }
    }
}