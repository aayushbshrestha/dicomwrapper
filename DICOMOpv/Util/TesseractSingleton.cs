﻿using System;
using System.Linq;
using Tesseract;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Util
{
    /// <summary>
    /// experimental class for potential optimization
    /// </summary>
    public sealed class TesseractSingleton
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private TesseractSingleton()
        {
        }

        static TesseractSingleton()
        {
        }

        private Language Language { get; set; }
        private static readonly TesseractSingleton instance = new TesseractSingleton();
        public static TesseractSingleton Instance { get { return instance; } }
        private static TesseractEngine engine;

        private static TesseractEngine GetEngine()
        {
            return engine;
        }

        private void InitEngine()
        {
            if (engine != null)
            {
                return;
            }
            else
            {
                log.Info("Initializing Tesseract Engine....");
                if (Language == Language.JP)
                {
                    engine = new TesseractEngine(@"./tessdata", "jpn_fast", EngineMode.Default);
                }
                else
                {
                    engine = new TesseractEngine(@"./tessdata", "eng_fast", EngineMode.Default);
                }
                engine.SetVariable("user_defined_dpi", "300");
            }
        }

        /// <summary>
        /// Setting final model for improving matrix data detection accuracy
        /// </summary>
        private void SetNewEngine()
        {
            engine = new TesseractEngine(@"./tessdata", "vf-final-best", EngineMode.Default);
            engine.SetVariable("user_defined_dpi", "300");
        }

        public void SetLanguage(Language _language)
        {
            Language = _language;
        }

        public string GetText(byte[] data)
        {
            try
            {
                InitEngine();
                using (var img = Pix.LoadFromMemory(data))
                {
                    using (var page = engine.Process(img, PageSegMode.SingleBlock))
                    {
                        float meanConfidence = page.GetMeanConfidence();
                        if (meanConfidence < 0.60)
                        {
                            string text = page.GetText();
                            if (string.IsNullOrEmpty(text))
                                return text;
                            else if (text.Any(char.IsDigit))
                            {
                                return SanitizeString(text);
                            }
                            return "unkown";
                        }
                        return SanitizeString(page.GetText());
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        public string ProcessPage(byte[] data)
        {
            try
            {
                InitEngine();
                using (var img = Pix.LoadFromMemory(data))
                {
                    using (var page = engine.Process(img, PageSegMode.SparseText))
                    {
                        return page.GetText();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
            finally
            {
                SetNewEngine();
            }
        }

        public string ProcessPage(byte[] data, Language lang)
        {
            try
            {
                InitEngine();
                using (var img = Pix.LoadFromMemory(data))
                {
                    using (var page = engine.Process(img, PageSegMode.SparseText))
                    {
                        return page.GetText();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
            finally
            {
                SetNewEngine();
            }
        }

        private static string SanitizeString(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            if (input.Contains("\n"))
                input = input.Replace("\n", "");

            if (input.Contains("="))
                input = input.Replace("=", "-");

            if (input.Contains("."))
                input = input.Replace(".", "-");

            if (input.Contains("'"))
                input = input.Replace("'", "-");

            if (input.Contains(","))
                input = input.Replace(",", "");

            if (input.Contains("‘"))
                input = input.Replace("‘", "");

            if (input.Contains("O"))
                input = input.Replace("O", "0");
            if (input.Count(negSign => negSign == '-') > 1)
            {
                input = input.Substring(0, input.Length - 1);
            }
            // edge case for 3 digit numbers for erroneous first digit values
            // Todo: Use image manipulation to get rid of line dividers that cause this issue.
            if (int.TryParse(input, out int number) && number > 100)
            {
                number = number % 100;
                input = number.ToString();
            }
            return input;
        }
    }
}