﻿using DICOMOpv.Models;
using DICOMOpv.Processors;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using Topcon.DicomWrapper;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Util
{
    internal class SkiaSharpHelper
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static ValueMatrix QuadrantGenerator(string rootFolder, byte[] imageBytes, string type, ReportType report)
        {
            try
            {
                log.Info($"Generating quadrants for {type}");
                ValueMatrix vm = new ValueMatrix();
                //this will be removed since in the actual application we don't write to memory
                var bitmap = SKBitmap.Decode(imageBytes);
                var image = SKImage.FromBitmap(bitmap);
                for (int quadrant = 0; quadrant < 4; quadrant++)
                {
                    //rectangle coordinates based on quadrant
                    int left = (quadrant + 1) % 2 == 0 ? image.Width / 2 : 0;
                    int top = (quadrant > 1) ? image.Height / 2 : 0;

                    int right = (quadrant + 1) % 2 == 0 ? image.Width : image.Width / 2;
                    int bottom = (quadrant > 1) ? image.Height : image.Height / 2;

                    var rect = new SKRectI(left, top, right, bottom);

                    var subset = image.Subset(rect);
                    // encode the image
                    var encodedData = subset.Encode();
                    AssignMatrixValues(ExtractCellsFromQuadrant(encodedData.AsStream(), report), quadrant, vm);
                }
                vm.SetType(type);
                return vm;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Quadrant generator for sensitivity value matrix
        /// </summary>
        /// <param name="rootFolder"></param>
        /// <param name="path">Source Image</param>
        /// <param name="type">Name of Matrix</param>
        /// <param name="report">Details of report being processed</param>
        /// <param name="laterality">Eye for identifying blind spot</param>
        /// <returns></returns>
        public static ValueMatrix QuadrantGenerator(string rootFolder, byte[] imageBytes, string type, ReportType report, Laterality laterality, bool hasTriangle)
        {
            try
            {
                log.Info($"Generating quadrants for {type}");
                ValueMatrix vm = new ValueMatrix();
                //this will be removed since in the actual application we don't write to memory
                var bitmap = SKBitmap.Decode(imageBytes);
                var image = SKImage.FromBitmap(bitmap);
                for (int quadrant = 0; quadrant < 4; quadrant++)
                {
                    var subset = image.Subset(GenerateRect(quadrant, image, report.DeviceType));
                    // encode the image
                    var encodedData = subset.Encode(SKEncodedImageFormat.Png, 75);
                    AssignMatrixValues(ExtractCellsFromQuadrant(encodedData.AsStream(), report, quadrant, laterality, hasTriangle), quadrant, vm);
                }
                vm.SetType(type);
                return vm;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        private static SKRectI GenerateRect(int quadrant, SKImage image, DeviceType device)
        {
            int left = 0, right = 0, top = 0, bottom = 0;
            int verticalLinePadding = 6;
            int horizontalLinePadding = 7;
            int horizontalLinePadding_lower = 15;

            if (device != DeviceType.HFA3)
            {
                verticalLinePadding = 12;
                horizontalLinePadding = 10;//15;
            }

            left = (quadrant + 1) % 2 == 0 ? (image.Width / 2) + verticalLinePadding : 0;
            top = (quadrant > 1) ? (image.Height / 2) + horizontalLinePadding : 0;
            if (device != DeviceType.HFA3)
                top = (quadrant > 1) ? (image.Height / 2) + horizontalLinePadding_lower : 0;

            right = (quadrant + 1) % 2 == 0 ? image.Width : (image.Width / 2) - verticalLinePadding;

            bottom = (quadrant > 1) ? image.Height : (image.Height / 2) - horizontalLinePadding;

            return new SKRectI(left, top, right, bottom);
        }

        private static void AssignMatrixValues(List<QuadrantData> data, int quadrant, ValueMatrix vm)
        {
            switch (quadrant)
            {
                case 0:
                    vm.quad1 = new Quadrant1(data);
                    vm.quad1.ConvertCoordinates();
                    break;

                case 1:
                    vm.quad2 = new Quadrant2(data);
                    vm.quad2.ConvertCoordinates();
                    break;

                case 2:
                    vm.quad3 = new Quadrant3(data);
                    vm.quad3.ConvertCoordinates();
                    break;

                case 3:
                    vm.quad4 = new Quadrant4(data);
                    vm.quad4.ConvertCoordinates();
                    break;
            }
        }

        private static void AssignMatrixValues(List<QuadrantData> data, int quadrant, SymbolMatrix vm)
        {
            switch (quadrant)
            {
                case 0:
                    vm.quad1 = new Quadrant1(data);
                    vm.quad1.ConvertCoordinates();
                    break;

                case 1:
                    vm.quad2 = new Quadrant2(data);
                    vm.quad2.ConvertCoordinates();
                    break;

                case 2:
                    vm.quad3 = new Quadrant3(data);
                    vm.quad3.ConvertCoordinates();
                    break;

                case 3:
                    vm.quad4 = new Quadrant4(data);
                    vm.quad4.ConvertCoordinates();
                    break;
            }
        }

        /// <summary>
        /// Take a quadrant from the 4x4 matrix and extract all cells present in that quadrant
        /// in a 5x5 matrix format
        /// TODO : optimize to avoid processing an extra row of information
        /// for upper quadrants skip first row and for lower quadrants skip last row since the first and last row in those cases is always empty
        /// for Total and Pattern Deviation Value matrices.
        /// The whole quadrant is currently taken in order to have uniform cell sizes.
        /// </summary>
        /// <param name="Quadrant">Region from which to extract data</param>
        /// <param name="device">Device type for configuring axis size</param>
        public static List<QuadrantData> ExtractCellsFromQuadrant(Stream Quadrant, ReportType report)
        {
            try
            {
                List<QuadrantData> CellValues = new List<QuadrantData>();

                var image = SKImage.FromBitmap(SKBitmap.Decode(Quadrant));
                int cellHeight = image.Height / 5;
                int cellWidth = image.Width / 5;
                int left = 10; // offset for axis lines
                if (report.DeviceType == DeviceType.HFA3) //adjustment for HFA3
                    left = 5;
                int right = image.Width % 10;
                int top = 5;
                int bottom = image.Height % 10;
                if (report.TemplateType == TemplateType.T10_2_HFAIIi || report.TemplateType == TemplateType.T30_2_HFAIIi || report.TemplateType == TemplateType.T30_2_GPA)
                    bottom += 5;
                //asuming a 5x5 matrix distribution
                for (int row = 0; row < 5; row++)
                {
                    for (int column = 0; column < 5; column++)
                    {
                        //values needed to be adjusted for last cell
                        var rect = new SKRectI(left, (bottom + cellHeight) <= image.Height ? top : image.Height - cellHeight, right + cellWidth, (bottom + cellHeight) <= image.Height ? (bottom + cellHeight) : image.Height - 2);
                        var subset = image.Subset(rect);
                        //update left and right positions
                        left += cellWidth;
                        right += cellWidth;
                        // encode the image
                        var encodedData = subset.Encode();
                        var memStream = new MemoryStream();
                        encodedData.AsStream().CopyTo(memStream);
                        CellValues.Add(new QuadrantData() { value = TesseractSingleton.Instance.GetText(memStream.ToArray()), row = row, col = column });
                    }

                    left = report.DeviceType == DeviceType.HFA3 ? 5 : 10; right = image.Width % 10; // reset to begining of matrix
                    top = bottom += cellHeight;
                }
                return CellValues;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        public static List<QuadrantData> ExtractCellsFromQuadrant(Stream Quadrant, ReportType report, int quadrantNumber, Laterality eye, bool hasTriangle = false)
        {
            try
            {
                List<QuadrantData> CellValues = new List<QuadrantData>();

                var image = SKImage.FromBitmap(SKBitmap.Decode(Quadrant));
                int cellHeight = image.Height / 5;
                int cellWidth = image.Width / 5;
                //baseline
                int left = 0;
                int top = 0;

                //asuming a 5x5 matrix distribution
                for (int row = 0; row < 5; row++)
                {
                    for (int column = 0; column < 5; column++)
                    {
                        //values needed to be adjusted for last cell
                        var rect = new SKRectI(left, top,
                                                (left + cellWidth) <= image.Width ? (left + cellWidth) : image.Width,
                                                (top + cellHeight) <= image.Height ? (top + cellHeight) : image.Height
                                              );

                        var subset = image.Subset(rect);

                        //update left and right positions
                        left += cellWidth;
                        // encode the image
                        var encodedData = subset.Encode(SKEncodedImageFormat.Png, 75);
                        var memStream = new MemoryStream();
                        encodedData.AsStream().CopyTo(memStream);
                        //for triangle present in svm, ignore value touched by the triangle, at position
                        //(-1,3) for right eye and (-1,-3) for left
                        //currently only lower values are being ignored might have to adjust for upper values in the future as well
                        if (eye == Laterality.Left && quadrantNumber == 2 && row == 0 && column == 2)
                        {
                            if (hasTriangle)
                                CellValues.Add(new QuadrantData() { value = "Blind Spot", row = row, col = column });
                            else
                            {
                                var value = TesseractSingleton.Instance.GetText(memStream.ToArray());
                                if (int.TryParse(value, out int number))
                                {
                                    CellValues.Add(new QuadrantData() { value = number.ToString(), row = row, col = column });
                                }
                                else
                                    CellValues.Add(new QuadrantData() { value = "Blind Spot", row = row, col = column });
                            }
                        }
                        else if (eye == Laterality.Right && quadrantNumber == 3 && row == 0 && column == 2)
                        {
                            if (hasTriangle)
                                CellValues.Add(new QuadrantData() { value = "Blind Spot", row = row, col = column });
                            else
                            {
                                var value = TesseractSingleton.Instance.GetText(memStream.ToArray());
                                if (int.TryParse(value, out int number))
                                {
                                    CellValues.Add(new QuadrantData() { value = number.ToString(), row = row, col = column });
                                }
                                else
                                    CellValues.Add(new QuadrantData() { value = "Blind Spot", row = row, col = column });
                            }
                        }
                        else
                        {
                            CellValues.Add(new QuadrantData() { value = TesseractSingleton.Instance.GetText(memStream.ToArray()), row = row, col = column });
                        }
                    }

                    //left = device == Topcon.DicomWrapper.DeviceType.HFA3 ? 5 : 10; right = image.Width % 10; // reset to begining of matrix
                    left = 5; // reset to begining of matrix
                    top += cellHeight;
                }
                return CellValues;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        public static SymbolMatrix QuadrantGeneratorForSymbolMatrix(string rootFolder, string path, string type, ReportType report)
        {
            try
            {
                SymbolMatrix sm = new SymbolMatrix();
                log.Info($"Creating {type} symbol matrix.");
                sm.SetType(type);
                //this will be removed since in the actual application we don't write to memory
                var bitmap = SKBitmap.Decode(path);
                var image = SKImage.FromBitmap(bitmap);
                for (int quadrant = 0; quadrant < 4; quadrant++)
                {
                    //rectangle coordinates based on quadrant
                    int left = (quadrant + 1) % 2 == 0 ? image.Width / 2 : 0;
                    int top = (quadrant > 1) ? image.Height / 2 : 0;

                    int right = (quadrant + 1) % 2 == 0 ? image.Width : image.Width / 2;
                    int bottom = (quadrant > 1) ? image.Height : image.Height / 2;

                    var rect = new SKRectI(left, top, right, bottom);

                    var subset = image.Subset(rect);
                    // encode the image
                    var encodedData = subset.Encode(SKEncodedImageFormat.Png, 75);
                    //can call cell extractor here
                    AssignMatrixValues(ExtractSymbolCellsFromQuadrant(encodedData.AsStream(), report.DeviceType), quadrant, sm);
                }
                return sm;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        public static List<QuadrantData> ExtractSymbolCellsFromQuadrant(Stream Quadrant, DeviceType device)
        {
            try
            {
                List<QuadrantData> CellValues = new List<QuadrantData>();

                var image = SKImage.FromBitmap(SKBitmap.Decode(Quadrant));
                int cellHeight = image.Height / 5;
                int cellWidth = image.Width / 5;
                int left = 0; // offset for axis lines
                left = 5; // resetting for hfa2 and hfa3
                if (device == DeviceType.HFA3) //adjustment for HFA3
                    left = 0;
                int right = image.Width % 10;
                int top = 5;
                int bottom = image.Height % 10;

                //asuming a 5x5 matrix distribution
                for (int row = 0; row < 5; row++)
                {
                    for (int column = 0; column < 5; column++)
                    {
                        //values needed to be adjusted for last cell
                        var rect = new SKRectI(left, (bottom + cellHeight) <= image.Height ? top : image.Height - cellHeight, (right + cellWidth) <= image.Width ? right + cellWidth : image.Width,
                                                    (bottom + cellHeight) <= image.Height ? (bottom + cellHeight) : image.Height - 2);
                        var subset = image.Subset(rect);
                        //update left and right positions
                        left += cellWidth;
                        right += cellWidth;
                        // encode the image
                        var encodedData = subset.Encode(SKEncodedImageFormat.Png, 75);
                        CellValues.Add(new QuadrantData() { value = SymbolProcessor.GetCellValue(encodedData.AsStream(), device), row = row, col = column });
                    }
                    // not checking for hfa 3 might have to adjust for those devices later
                    left = device == DeviceType.HFA3 ? 0 : 5; right = image.Width % 10; // reset to begining of matrix
                    top = bottom += cellHeight;
                }
                return CellValues;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }
    }
}