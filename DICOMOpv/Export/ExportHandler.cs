﻿using DICOMOpv.Serializer;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DICOMOpv.Export
{
    public class ExportHandler
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string Export(Report report, string OriginalFilePath, string outputFolder = null)
        {
            StringWriter stringWriter = null;
            XmlSerializer reportSerializer = new XmlSerializer(report.GetType());
            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            try
            {
                stringWriter = new StringWriter();
                using (XmlWriter writer = XmlWriter.Create(stringWriter, settings))
                {
                    reportSerializer.Serialize(writer, report);
                    string xml = stringWriter.ToString();
                    var _guid = GetGuidFromSource(OriginalFilePath);
                    StringBuilder fileName = new StringBuilder();
                    if (_guid != null)
                    {
                        fileName.Append(_guid.ToString());
                    }
                    fileName.Append($"{GetFileName(xml)}({DateTime.Now:yyyy-MM-dd_hh-mm-ss-fff})");

                    if (string.IsNullOrEmpty(outputFolder))
                    {
                        outputFolder = ConfigurationManager.AppSettings["FolderForXmlOutput"];
                    }
                    string outFilePath = Path.Combine(outputFolder, $"{fileName}.xml");
                    File.WriteAllText(outFilePath, xml, Encoding.Unicode);
                    stringWriter = null;
                    return outFilePath;
                }
            }
            catch (Exception ex)
            {
                log.Error($"Could not export xml file. {ex.Message}");
                return null;
            }
            finally
            {
                if (stringWriter != null)
                {
                    stringWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Guid tags are pre-fixed to file names
        /// The first 36 elements of the file name should contain the guid
        /// </summary>
        /// <returns></returns>
        private static Guid? GetGuidFromSource(string OriginalFilePath)
        {
            try
            {
                bool parsed = Guid.TryParse(OriginalFilePath.Substring(36), out Guid fileId);
                if (parsed)
                    return fileId;
                else
                    return null;
            }
            catch (Exception ex)
            {
                log.Error($"Could not parse GUID from file name = {OriginalFilePath}. Exception message = {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Gets the report file name.
        /// </summary>
        /// <param name="xml">Text value of the XML file.</param>
        /// <returns>Report file name.</returns>
        private static string GetFileName(string xml)
        {
            string[] _dateFormats = { "MMM dd, yyyy H:mm" };
            MemoryStream memoryStream = null;
            try
            {
                memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(xml));
                using (XmlReader xmlReader = XmlReader.Create(memoryStream))
                {
                    memoryStream = null;
                    char[] param = { '\r', '\n', ' ' };
                    XElement root = XElement.Load(xmlReader);
                    string deviceType = root.Descendants().Where(x => (string)x.Attribute("field") == "MachineType")
                               .FirstOrDefault().Value.Trim(param);
                    string patientIdKey = deviceType == "HFA3" ? "PatientId" : "ID";
                    string lateralityKey = deviceType == "HFA3" ? "Laterality" : "Eye";
                    string patientId = root.Descendants().Where(x => (string)x.Attribute("field") == patientIdKey)
                                        .FirstOrDefault().Value.Trim(param).Replace(" ", string.Empty);
                    string testType = root.Descendants().Where(x => (string)x.Attribute("field") == "TestType")
                                        .FirstOrDefault().Value.Trim(param);
                    string laterality = root.Descendants().Where(x => (string)x.Attribute("field") == "Laterality")
                                        .FirstOrDefault().Value.Trim(param);
                    string testDateStr = root.Descendants().Where(x => (string)x.Attribute("field") == "TestDate")
                                        .FirstOrDefault().Value.Trim(param);
                    //if can't parse date set default date to now
                    DateTime testDate = DateTime.Now;
                    DateTime.TryParse(testDateStr, out testDate);
                    string fileType = root.Descendants().FirstOrDefault(x => (string)x.Attribute("field") == "FileType")
                                        .Value.Trim(param);
                    return $"{patientId}-{testType}-{laterality}-{testDate:yyyyMMdd}-{testDate:HHmm}-{GetDeviceType(deviceType)}-{fileType}";
                }
            }
            catch (Exception ex)
            {
                log.Error($"Could not construct file name. {ex.Message}");
                return null;
            }
            finally
            {
                if (memoryStream != null)
                {
                    memoryStream.Dispose();
                }
            }
        }

        private static string GetDeviceType(string deviceType)
        {
            return deviceType == "HFA3" ? "HFA3" : "HFA2";
        }
    }
}