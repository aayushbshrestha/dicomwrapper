﻿using OpenCvSharp;
using System;
using System.IO;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Processors
{
    public class SymbolProcessor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string GetCellValue(Stream cell, DeviceType device)
        {
            try
            {
                string symbolsFolder = @".\Resources\Generated Symbol\hfa3\";
                if (device == DeviceType.HFA2 || device == DeviceType.HFAIIi)
                    symbolsFolder = @".\Resources\Generated Symbol\hfa2\";
                Mat original = Mat.FromStream(cell, ImreadModes.Color);
                foreach (string symbol in Directory.GetFiles(symbolsFolder))
                {
                    if (MatchTemplate(original, symbol))
                    {
                        return GetSymbolStringValue(symbol);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        public static bool HasTriangle(string matrix)
        {
            string TrianglesFolder = @".\Resources\Generated Symbol\Triangles\";
            Mat original = new Mat(matrix);
            foreach (string triangle in Directory.GetFiles(TrianglesFolder))
            {
                if (MatchTemplate(original, triangle))
                {
                    return true;
                }
            }
            return false;
        }

        private static string GetSymbolStringValue(string symbol)
        {
            try
            {
                symbol = Path.GetFileNameWithoutExtension(symbol);
                switch (symbol)
                {
                    case "less_than_five":
                        return "<5";

                    case "less_than_one":
                        return "<1";

                    case "less_than_two":
                    case "less_than_two_v2":
                        return "<2";

                    case "less_than_zero_five":
                    case "less_than_zero_five_v1":
                        return "<0.5";

                    case "not_less_than_five":
                    case "not_less_than_five_v2":
                        return ">5";
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        private static bool MatchTemplate(Mat original, string symbol)
        {
            Mat blur = new Mat();
            Cv2.GaussianBlur(original, blur, new Size(5, 5), 0);
            using (Mat symbolToMatch = new Mat(symbol))
            {
                //defining matching parameters
                using (Mat res = new Mat(blur.Rows - symbolToMatch.Rows + 1, blur.Cols - symbolToMatch.Cols + 1, MatType.CV_32FC1))
                {
                    //convert to greyscale
                    Mat gOriginal = original.CvtColor(ColorConversionCodes.BGR2GRAY);
                    Mat gSymbol = symbolToMatch.CvtColor(ColorConversionCodes.BGR2GRAY);

                    //perform matching
                    Cv2.MatchTemplate(gOriginal, gSymbol, res, TemplateMatchModes.CCoeffNormed);
                    Cv2.Threshold(res, res, 0.8, 1.0, OpenCvSharp.ThresholdTypes.Tozero);

                    double threshold = 0.8;
                    Cv2.MinMaxLoc(res, out double minval, out double maxval, out Point minloc, out Point maxloc);

                    if (maxval >= threshold)
                        return true;
                }
            }

            return false;
        }

        private static bool RunTemplateMatching(Stream source, string symbol)
        {
            using (Mat original = Mat.FromStream(source, ImreadModes.Color))
            {
                Mat blur = new Mat();
                Cv2.GaussianBlur(original, blur, new Size(5, 5), 0);
                using (Mat symbolToMatch = new Mat(symbol))
                {
                    //defining matching parameters
                    using (Mat res = new Mat(blur.Rows - symbolToMatch.Rows + 1, blur.Cols - symbolToMatch.Cols + 1, MatType.CV_32FC1))
                    {
                        //convert to greyscale
                        Mat gOriginal = original.CvtColor(ColorConversionCodes.BGR2GRAY);
                        Mat gSymbol = symbolToMatch.CvtColor(ColorConversionCodes.BGR2GRAY);

                        //perform matching
                        Cv2.MatchTemplate(gOriginal, gSymbol, res, TemplateMatchModes.CCoeffNormed);
                        Cv2.Threshold(res, res, 0.8, 1.0, OpenCvSharp.ThresholdTypes.Tozero);

                        double threshold = 0.8;
                        Cv2.MinMaxLoc(res, out double minval, out double maxval, out Point minloc, out Point maxloc);

                        if (maxval >= threshold)
                            return true;
                    }
                }
            }
            return false;
        }
    }
}