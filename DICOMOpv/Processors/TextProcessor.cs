﻿using DICOMOpv.Models;
using DICOMOpv.Serializer;
using DICOMOpv.Serializer.ReportPurifiers;
using ImageMagick;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Topcon.DicomWrapper;
using static DICOMOpv.Models.Enums.EnumCollection;
using static DICOMOpv.Models.ComponentConfiguration;

namespace DICOMOpv.Util
{
    public class TextProcessor
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string[] ESCAPE_CHAR = new string[] { "\n\n", "\r\n", "\r", "\n" };

        /// <summary>
        /// Loads appropriate template to apply regex for text label extraction
        /// </summary>
        /// <param name="path">Template path</param>
        /// <returns></returns>
        private static List<TextConfiguration> LoadConfig(string path)
        {
            try
            {
                log.Info($"Loading Configuration from {path}.");
                string value = File.ReadAllText(path, Encoding.UTF7);
                var template = JsonConvert.DeserializeObject<BaseTemplate>(value);
                DicomOpv.SetMachineTypeSerializer(template.MachineType);
                return (List<TextConfiguration>)template.TextComponentConfigurations;
            }
            catch (Exception ex)
            {
                log.Error("Could not Load Config for Text components. Ex Message: " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Generates Text Item List to be used for serialization
        /// </summary>
        /// <param name="reportText"></param>
        /// <param name="report"></param>
        /// <returns></returns>
        public static IList<TextItem> LoadTextTags(string reportText, ReportType report)
        {
            var reportTextList = reportText.Split(ESCAPE_CHAR, StringSplitOptions.RemoveEmptyEntries).Select(line => line.Trim());
            var config = LoadConfig(GetConfigPath(report));
            return GetTextItems(GetValuesForConfig(string.Join(Environment.NewLine, reportTextList.ToArray()), config), config);
        }

        /// <summary>
        /// Generates Text Item List to be used for serialization along with the Laterality for pdf/jpg input
        /// </summary>
        /// <param name="reportText"></param>
        /// <param name="report">Report object for template</param>
        /// <param name="isImage">identifier for pdf or jpg input</param>
        /// <returns></returns>
        public static (IList<TextItem>, Laterality) LoadTextTags(string reportText, ReportType report, bool isImage)
        {
            var reportTextList = reportText.Split(ESCAPE_CHAR, StringSplitOptions.RemoveEmptyEntries).Select(line => line.Trim());
            var config = LoadConfig(GetConfigPath(report));
            return GetTextItems(GetValuesForConfig(string.Join(Environment.NewLine, reportTextList.ToArray()), config), config, isImage);
        }

        private static IList<TextItem> GetTextItems(Dictionary<string, string> values, List<TextConfiguration> configs)
        {
            IList<TextItem> reportItems = new List<TextItem>();
            try
            {
                foreach (var config in configs)
                {
                    var item = values.FirstOrDefault(x => x.Key == config.Key);
                    if (item.Key != null)
                    {
                        string pairValue = item.Value;
                        TextItem textItm = GetTextItem(config, pairValue);
                        reportItems.Add(textItm);
                        values.Remove(item.Key);
                    }
                    else
                    {
                        log.Error($"Item not found for config {config.Key}");
                    }
                }
                Normalize(reportItems, configs);
                return reportItems;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return reportItems;
            }
        }

        private static (IList<TextItem>, Laterality) GetTextItems(Dictionary<string, string> values, List<TextConfiguration> configs, bool getLaterality)
        {
            IList<TextItem> reportItems = new List<TextItem>();
            Laterality lat = Laterality.Unknown;
            try
            {
                foreach (var config in configs)
                {
                    var item = values.FirstOrDefault(x => x.Key == config.Key);
                    if (item.Key == "Laterality")
                    {
                        lat = GetLateralityFromString(item.Value);
                    }
                    if (item.Key != null)
                    {
                        string pairValue = item.Value;
                        TextItem textItm = GetTextItem(config, pairValue);
                        reportItems.Add(textItm);
                        values.Remove(item.Key);
                    }
                    else
                    {
                        log.Error($"Item not found for config {config.Key}");
                    }
                }
                Normalize(reportItems, configs);
                return (reportItems, lat);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return (reportItems, Laterality.Unknown);
            }
        }

        private static Laterality GetLateralityFromString(string eye)
        {
            try
            {
                if (eye == "OD" || eye == "RIGHT" || eye == "R" || eye.ToLower() == "derecho") //Todo : Add other variations for spanish language
                {
                    return Laterality.Right;
                }
                else if (eye == "OS" || eye == "LEFT" || eye == "L")
                {
                    return Laterality.Left;
                }
                else if (eye == "OU" || eye == "RL" || eye == "LR")
                {
                    return Laterality.Both;
                }
                else
                    return Laterality.Unknown;
            }
            catch (Exception ex)
            {
                log.Error($"Error encountered trying to get laterality for value = {eye}. Exception : {ex.Message}");
                return Laterality.Unknown;
            }
        }

        private static void Normalize(IList<TextItem> item, List<TextConfiguration> configs)
        {
            foreach (var config in configs)
            {
                if (!string.IsNullOrEmpty(config.Purifier))
                {
                    try
                    {
                        PurifierFactory.Get(config.Purifier).Purify(item, config.Key);
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Could not purify {config.Key} value. Exception : {ex.Message}");
                    }
                }
            }
        }

        private static TextItem GetTextItem(TextConfiguration config, string text)
        {
            Dictionary<string, string> values = null;
            if (string.IsNullOrWhiteSpace(config.ProjectionPattern))
            {
                values = new Dictionary<string, string>()
                {
                    { "Value", text },
                };
            }
            else
            {
                values = GetProjectionPattern(config.ProjectionPattern, text, config.ProjectionElements);
            }

            TextItem textItem = new TextItem(config.Key, values, config.SerializationKey);
            if (!string.IsNullOrEmpty(config.OriginalKey))
                textItem.SetOriginalKey(config.OriginalKey);

            return textItem;
        }

        private static string GetConfigPath(ReportType report)
        {
            string path = "./Template";
            if (report.DetectedLanguage != Language.EN)
            {
                path += $"/{report.DetectedLanguage.ToString()}";
            }
            var newpath = Path.Combine(path, report.TemplateType.ToString() + ".json");
            return newpath;
        }

        /// <summary>
        /// Sanitizes text values for tesseract errors
        /// </summary>
        /// <param name="value"></param>
        private static void SanitizeMatch(ref string value)
        {
            value = value.ToLower();
            if (value == ".00")
                value = "OD";
            else if (value == ".03")
                value = "OS";
            else if (value == "od" || value == "op")
                value = "OD";
            else if (value == "os")
                value = "OS";
            else if (value.Contains("—"))
                value = value.Replace('—', '-');
        }

        /// <summary>
        /// Gets all the values corresponding to the each key in <see cref="TextConfiguration"/> list.
        /// </summary>
        /// <param name="reportText">The content of the report as a string.</param>
        /// <param name="configs">A list of <see cref="TextConfiguration"/>.</param>
        /// <returns>A dictionary of extracted text values with their corresponding keys.</returns>
        private static Dictionary<string, string> GetValuesForConfig(string reportText, IList<TextConfiguration> configs)
        {
            try
            {
                Dictionary<string, string> extracValues = new Dictionary<string, string>();
                List<string> unmatchedKeys = new List<string>();
                foreach (TextConfiguration config in configs)
                {
                    if (!string.IsNullOrWhiteSpace(config.LookupRegx))
                    {
                        string pattern = config.LookupRegx;
                        if (config.LookupRegx.Contains("Central 24"))
                        {
                            pattern = pattern.Replace("-", @"\W");
                        }
                        Regex regex = new Regex(pattern, RegexOptions.Multiline);
                        Match match = regex.Match(reportText);

                        if (match.Success)
                        {
                            string val = match.Groups["value"].Value.Trim();
                            SanitizeMatch(ref val);
                            //sanitizing for OD/OS for HFA3 reports
                            extracValues.Add(config.Key, val);
                        }
                        else if (config.Key == "RX")
                        {
                            string val = RXExtractor(reportText, config);
                            if (!string.IsNullOrEmpty(val))
                                extracValues.Add(config.Key, val);
                        }
                        else
                            unmatchedKeys.Add(config.Key);
                    }
                    else if (config.EmbedKeys.Any() && !config.NextKeys.Any())
                    {
                        string startValues = string.Join("|", config.EmbedKeys.ToArray());
                        string partten = $"(?<start>{startValues})(?<value>.*)";
                        Regex regex = new Regex(partten, RegexOptions.Multiline);
                        Match match = regex.Match(reportText);

                        if (match.Success)
                        {
                            string val = match.Groups["value"].Value.Trim();
                            extracValues.Add(config.Key, val);
                        }
                        else
                        {
                            if (config.Key.Equals("PatientId") /* && Configurations.SkipPatientId*/)
                            {
                                string val = "autId" + DateTime.Now.DayOfYear + DateTime.Now.Millisecond;
                                extracValues.Add(config.Key, val);
                            }
                            else
                                unmatchedKeys.Add(config.Key);
                        }
                    }
                    else if (config.EmbedKeys.Any() && config.NextKeys.Any())
                    {
                        List<string> parttens = GetParttens(config.EmbedKeys, config.NextKeys);
                        bool found = false;
                        foreach (string partten in parttens)
                        {
                            Regex regex = new Regex(partten, RegexOptions.Multiline);
                            Match match = regex.Match(reportText);

                            if (match.Success)
                            {
                                found = true;
                                string val = match.Groups["value"].Value.Trim();
                                extracValues.Add(config.Key, val);
                                break;
                            }
                        }
                        if (!found)
                        {
                            unmatchedKeys.Add(config.Key);
                        }
                    }//TODO: Verify if all keys are being read before hitting this path
                    else if (!string.IsNullOrWhiteSpace(config.ProjectionPattern))
                    {
                        var values = GetProjectionPattern(config.ProjectionPattern, reportText, config.ProjectionElements);
                        foreach (var item in values)
                            extracValues.Add(item.Key, values[item.Key]);
                    }
                }

                return extracValues;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        private static string RXExtractor(string reportText, TextConfiguration config)
        {
            string tmp = reportText.Replace("\r\n", "");
            string partten = config.LookupRegx;
            Regex regex = new Regex(partten, RegexOptions.Multiline);
            Match match = regex.Match(tmp);

            if (match.Success)
            {
                return match.Groups["value"].Value.Trim();
            }
            return null;
        }

        /// <summary>
        /// Gets a list of regex pattern generated using given two key lists.
        /// </summary>
        /// <param name="embedKeys">The list of starting keywords.</param>
        /// <param name="nextKeys">The list of ending keywords.</param>
        /// <returns>A list of regex strings.</returns>
        private static List<string> GetParttens(IList<string> embedKeys, IList<string> nextKeys)
        {
            List<string> parttens = new List<string>();

            foreach (string embedKey in embedKeys)
            {
                foreach (string nextKey in nextKeys)
                {
                    string parten = $"(?<start>{embedKey}\r\n)(?<value>.*)(?<end>\r\n{nextKey})";
                    parttens.Add(parten);
                }
            }

            return parttens;
        }

        private static Dictionary<string, string> GetProjectionPattern(string projectionPattern, string textValue, List<string> projectionElements)
        {
            Regex regex = new Regex(projectionPattern);

            Match match = regex.Match(textValue);

            Dictionary<string, string> values = new Dictionary<string, string>();

            if (match.Success)
            {
                for (int i = 0; i <= projectionElements.Count - 1; i++)
                {
                    string elementName = projectionElements[i];
                    values.Add(elementName, match.Groups[elementName].Value.Trim());
                }
            }
            else
            {
                log.Error("Could not match projection pattern.");
                throw new Exception("Projection pattern did not match");
            }

            return values;
        }

        public static byte[] ReturnImageBytesFromPdf(byte[] buffer)
        {
            try
            {
                var image = new MagickImage(buffer, new MagickReadSettings() { Density = new Density(300) });
                //converting format to png
                image.Format = MagickFormat.Png;
                return image.ToByteArray();
            }
            catch (Exception ex)
            {
                log.Error($"Could not convert pdf to image.\nException Message : {ex.Message}.");
                return null;
            }
        }

        private static Stream ReturnImageStreamFromPdf(byte[] buffer)
        {
            try
            {
                log.Info("Extracting image bytes from pdf object.");
                MemoryStream ms = new MemoryStream();
                MagickReadSettings settings = new MagickReadSettings();
                // Settings the density to 300 dpi will create an image with a better quality which will help with OCR accuracy
                settings.Density = new Density(300);

                using (MagickImageCollection images = new MagickImageCollection())
                {
                    // Add all the pages of the pdf file to the collection
                    images.Read(new MemoryStream(buffer), settings);
                    if (images.Count == 1)
                    {
                        var image = images[0];
                        image.Format = MagickFormat.Png;
                        image.Write(ms); //we will most likely only have a single page here
                    }
                    else
                    {
                        // this scenario is possible but is not currently supported
                        foreach (MagickImage image in images)
                        {
                            // Write page to file that contains the page number
                            image.Format = MagickFormat.Png;
                            image.Write(ms); //potentiall return a list of images as byte arrays
                        }
                    }
                }

                return ms;
            }
            catch (Exception ex)
            {
                log.Error($"Failed during image byte data extraction {ex.Message}");
                return null;
            }
        }
    }
}