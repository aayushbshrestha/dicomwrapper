﻿using DICOMOpv.Models;
using DICOMOpv.Processors;
using ImageMagick;
using log4net;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using Topcon.DicomWrapper;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Util
{
    public class ImageProcessor
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Dictionary<TemplateType, int> TEMPLATE_BRIGHTNESS_VALUE = new Dictionary<TemplateType, int>()
            {
                { TemplateType.T10_2, 2 },
                { TemplateType.T10_2_HFAIIi, 2 },
                { TemplateType.T24_2, 0 },
                { TemplateType.T24_2_HFAIIi, 2 },
                { TemplateType.T30_2, 2 },
                { TemplateType.T30_2_HFAIIi, 2 },
                { TemplateType.T24_2_GPA, 2 },
                { TemplateType.T30_2_GPA, 2 },
                { TemplateType.T30_2_HFAIIi_GPA, 2 }
            };

        private static Dictionary<TemplateType, int> TEMPLATE_THRESHOLD_VALUE = new Dictionary<TemplateType, int>()
            {
                { TemplateType.T10_2, 99 },
                { TemplateType.T10_2_HFAIIi, 50 },
                { TemplateType.T24_2, 71 },
                { TemplateType.T24_2_HFAIIi, 95 },
                { TemplateType.T30_2, 85 },
                { TemplateType.T30_2_GPA, 85 },
                { TemplateType.T30_2_HFAIIi, 95 },
                { TemplateType.T30_2_HFAIIi_GPA, 95 },
                { TemplateType.T24_2_GPA, 71 },
            };

        private static MagickImage GetImageFromPdfBuffer(byte[] buffer)
        {
            MagickReadSettings settings = new MagickReadSettings();
            // Settings the density to 300 dpi will create an image with a better quality which will help with OCR accuracy
            settings.Density = new Density(300);
            return new MagickImage(buffer, settings);
        }

        /// <summary>
        /// this method generates invokes all the methods required to generate files to serve the purpose of data extraction
        /// </summary>
        /// <param name="pathToImage"></param>
        /// <param name="type"></param>
        /// <param name="outputDir"></param>
        /// <returns></returns>
        public static string GenerateHelperImages(byte[] pdf_data, ReportType type, bool hasImageBytes = false)
        {
            // add log here (generated images)
            var guid = Guid.NewGuid();
            var path = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["RootFolderForTempImages"], Guid.NewGuid().ToString());
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return EnhancedImage(pdf_data, path, type, hasImageBytes);
        }

        /// <summary>
        /// Generates all necessary quadrant information for value matrices and stores them in a list
        /// </summary>
        /// <param name="pathToEnhancedImage"></param>
        /// <param name="report"></param>
        /// <returns>string path to potentially usable image artifacts</returns>
        /// <returns>List of evaluated value matrices</returns>
        public static (string, List<ValueMatrix>, List<SymbolMatrix>, Dictionary<string, string>) GenerateAccesories(string pathToEnhancedImage, ReportType report, Laterality laterality)
        {
            bool isSymbolMatrix = false;
            List<ValueMatrix> valueMatrices = new List<ValueMatrix>();
            List<SymbolMatrix> symbolMatrices = new List<SymbolMatrix>();
            Dictionary<string, string> images = new Dictionary<string, string>();
            var coordinates = CoordinateResolver.GetCoordinates(report.TemplateType);
            var rootFolder = pathToEnhancedImage.Substring(0, pathToEnhancedImage.Length - "EnhancedImage.png".Length);
            foreach (var points in coordinates)
            {
                if (points.Key == Constants.PATTERN_DEV_SYMBOL_MATRIX || points.Key == Constants.TOTAL_DEVIATION_SYMBOL_MATRIX)
                    isSymbolMatrix = true;
                else
                    isSymbolMatrix = false;
                if (isSymbolMatrix)
                {
                    symbolMatrices.Add(GenerateSymbolQuadrants(rootFolder, SaveImagePortion(points.Value, pathToEnhancedImage, points.Key, isSymbolMatrix), points.Key, report));
                }
                else if (points.Key == Constants.GAZE || points.Key == Constants.GREY_SCALE)
                {
                    var imgBytes = GetImageBytes(points.Value, pathToEnhancedImage);
                    images.Add(points.Key, Convert.ToBase64String(imgBytes));
                }
                else if (points.Key == Constants.PATTERN_DEV_VALUE_MATRIX
                    || points.Key == Constants.TOTAL_DEVIATION_VALUE_MATRIX)
                {
                    var accessoryImageBytes = GetImageBytes(points.Value, pathToEnhancedImage);
                    valueMatrices.Add(GenerateQuadrants(rootFolder, accessoryImageBytes, points.Key, report, false));
                }
                else if (points.Key == Constants.SENSITIVITY_VALUE_MATRIX)
                {
                    var sens = GetImagePortionAsBytesForSensitivtyMatrix(points.Value, pathToEnhancedImage, points.Key, isSymbolMatrix);
                    bool hasTriangle = SymbolProcessor.HasTriangle(sens.Item2);
                    valueMatrices.Add(GenerateQuadrants(rootFolder, sens.Item1, points.Key, report, true, laterality, hasTriangle));
                }
                else
                {
                    log.Error($"Invalid type provided. Could not generate Accessory for key = {points.Key}");
                }
            }
            return (rootFolder, valueMatrices, symbolMatrices, images);
        }

        private static ValueMatrix GenerateQuadrants(string rootFolder, byte[] imageBytes, string type, ReportType report, bool isSvm, Laterality laterality = Laterality.Unknown, bool hasTriangle = false)
        {
            return !isSvm ? SkiaSharpHelper.QuadrantGenerator(rootFolder, imageBytes, type, report) : SkiaSharpHelper.QuadrantGenerator(rootFolder, imageBytes, type, report, laterality, hasTriangle);
        }

        private static SymbolMatrix GenerateSymbolQuadrants(string rootFolder, string imagePath, string type, ReportType report)
        {
            return SkiaSharpHelper.QuadrantGeneratorForSymbolMatrix(rootFolder, imagePath, type, report);
        }

        private void ReducedNoiseBlackAndWhiteImage(byte[] buffer, ReportType type, string path)
        {
            string BWId = "BlackAndWhite.png";
            using (var img = GetImageFromPdfBuffer(buffer))
            {
                int ThresholdValue = 92;
                img.Threshold(new Percentage(ThresholdValue));
                img.Write(Path.Combine(path, type.TemplateType.ToString() + BWId)); // this would go into a processing directory
                img.Dispose();
            }
        }

        /// <summary>
        /// this image is enhanced image with contrast and upped brightness
        /// </summary>
        /// <param name="image"></param>
        /// <param name="type"></param>
        private static string EnhancedImage(byte[] buffer, string path, ReportType report, bool hasImage = false)
        {
            try
            {
                const int contrast = 25;
                var outPath = Path.Combine(path, "EnhancedImage.png");
                if (hasImage)
                {
                    using (MagickImage img = new MagickImage(buffer))
                    {
                        img.BrightnessContrast(new Percentage(TEMPLATE_BRIGHTNESS_VALUE[report.TemplateType]), new Percentage(contrast));
                        img.Write(outPath);
                        img.Dispose();
                    }
                }
                else
                {
                    using (MagickImage img = GetImageFromPdfBuffer(buffer))
                    {
                        img.BrightnessContrast(new Percentage(TEMPLATE_BRIGHTNESS_VALUE[report.TemplateType]), new Percentage(contrast));
                        img.Write(outPath);
                        img.Dispose();
                    }
                }
                return outPath;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        private static byte[] GetImagePortionAsBytes(Coordinates coords, string imagePath)
        {
            try
            {
                using (MagickImage newImage = new MagickImage(imagePath))
                {
                    MagickGeometry crop = new MagickGeometry()
                    {
                        Height = coords.Height,
                        Width = coords.Width,
                        X = coords.Left,
                        Y = coords.Top
                    };
                    newImage.Crop(crop);
                    return newImage.ToByteArray();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        private static byte[] GetImageBytes(Coordinates coords, string imagePath)
        {
            try
            {
                SKBitmap bitmap = SKBitmap.Decode(imagePath);
                SKImage image = SKImage.FromBitmap(bitmap);
                SKRectI rect = new SKRectI(coords.Left, coords.Top, coords.Left + coords.Width, coords.Top + coords.Height);
                var subset = image.Subset(rect);
                var encoded = subset.Encode();
                return encoded.ToArray();
            }
            catch (Exception ex)
            {
                log.Error($"Could not decode image bytes. {ex.Message}");
                return null;
            }
        }

        private static (byte[], string) GetImagePortionAsBytesForSensitivtyMatrix(Coordinates coords, string imagePath, string region, bool isSymbol)
        {
            try
            {
                string fileName = region;
                if (isSymbol)
                    fileName += ".jpg";
                else
                    fileName += ".png";

                var path = Path.Combine(imagePath.Substring(0, imagePath.Length - "EnhancedImage.png".Length), fileName);

                using (MagickImage newImage = new MagickImage(imagePath))
                {
                    MagickGeometry crop = new MagickGeometry()
                    {
                        Height = coords.Height,
                        Width = coords.Width,
                        X = coords.Left,
                        Y = coords.Top
                    };
                    newImage.Crop(crop);
                    newImage.Write(path);
                    newImage.Format = MagickFormat.Png;
                    newImage.Quality = 100;
                    return (newImage.ToByteArray(), path);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return (null, null);
            }
        }

        //TODO : Verify if this can be used instead of current implementation for save symbols
        private static string SaveImagePortionFast(Coordinates coords, string imagePath, string region, bool isSymbol = false)
        {
            try
            {
                string fileName = region;
                if (isSymbol)
                    fileName += ".jpg";
                else
                    fileName += ".png";
                var path = Path.Combine(imagePath.Substring(0, imagePath.Length - "EnhancedImage.png".Length), fileName);
                using (SKImage image = SKImage.FromBitmap(SKBitmap.Decode(imagePath)))
                {
                    SKRectI rect = new SKRectI(coords.Left, coords.Top, coords.Left + coords.Width, coords.Top + coords.Height);
                    var subset = image.Subset(rect);
                    var bmp = SKBitmap.FromImage(subset);
                    var encoded = (isSymbol) ? subset.Encode(SKEncodedImageFormat.Jpeg, 100) : subset.Encode();
                    using (var fileStream = File.Create(path))
                    {
                        encoded.SaveTo(fileStream);
                    }
                }
                return path;
            }
            catch (Exception ex)
            {
                log.Error($"Couldn't save image portion for ${region}. Exception : {ex.Message}");
                return null;
            }
        }

        private static string SaveImagePortion(Coordinates coords, string imagePath, string region, bool isSymbol)
        {
            try
            {
                string fileName = region;
                if (isSymbol)
                    fileName += ".jpg";
                else
                    fileName += ".png";
                var path = Path.Combine(imagePath.Substring(0, imagePath.Length - "EnhancedImage.png".Length), fileName);
                using (MagickImage newImage = new MagickImage(imagePath))
                {
                    MagickGeometry crop = new MagickGeometry()
                    {
                        Height = coords.Height,
                        Width = coords.Width,
                        X = coords.Left,
                        Y = coords.Top
                    };
                    newImage.Crop(crop);
                    newImage.Write(path);
                    newImage.Dispose();
                }
                return path;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Cleanup all the images once processing is completed
        /// </summary>
        /// <param name="path">path to dir containing all helper images</param>
        public static void DisposeImages(string path)
        {
            if (Directory.Exists(path))
            {
                Array.ForEach(Directory.GetFiles(path), file => File.Delete(file)); // dispose of all files inside of the directory
                Directory.Delete(path);
            }
        }
    }
}