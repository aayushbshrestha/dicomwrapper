﻿using DICOMOpv.Models;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DICOMOpv.Processors
{
    public static class DateTimeConverter
    {
        /// <summary>
        /// US date format.
        /// </summary>
        private const string UsDateFormat = "MM-dd-yyyy";

        /// <summary>
        /// UK date format.
        /// </summary>
        private const string UkDateFormat = "dd-MM-yyyy";

        /// <summary>
        /// Potential time formats.
        /// </summary>
        private static readonly string[] TimeFormats = { "hh:mm tt", "hh:mmtt", "h:mm tt", "h:mmtt", "HH:mm" };

        /// <summary>
        /// UK time format.
        /// </summary>
        private static readonly string[] UkTimeFormat = { "HH:mm" };

        /// <summary>
        /// Converts test date and birthday to <see cref="DateTime"/> based on the test time format.
        /// </summary>
        /// <param name="birthdayText">The birth date as a string.</param>
        /// <param name="reportDateText">The test date as a string.</param>
        /// <param name="reportTimeText">The report time as a string.</param>
        /// <returns>An instance of <see cref="ReportDateTimesDto"/> class.</returns>
        public static ReportDateTimeDto Convert(string birthdayText, string reportDateText, string reportTimeText)
        {
            if (string.IsNullOrWhiteSpace(birthdayText))
            {
                throw new ArgumentException(nameof(birthdayText));
            }

            if (string.IsNullOrWhiteSpace(reportDateText))
            {
                throw new ArgumentException(nameof(reportDateText));
            }

            if (string.IsNullOrWhiteSpace(reportTimeText))
            {
                throw new ArgumentException(nameof(reportTimeText));
            }

            string dateFormat = GetDateFormat(birthdayText, reportDateText, reportTimeText);
            if (dateFormat == null)
            {
                dateFormat = UsDateFormat;
            }

            ReportDateTimeDto dateTimes = new ReportDateTimeDto();
            DateTime? birthday = GetDate(dateFormat, birthdayText, null);
            if (birthday.HasValue)
            {
                dateTimes.Birthday = birthday.Value;
            }

            DateTime? reportDate = GetDate(dateFormat, reportDateText, reportTimeText);
            if (reportDate.HasValue)
            {
                dateTimes.ReportDateTime = reportDate.Value;
            }

            return dateTimes;
        }

        /// <summary>
        /// Parses date string value with a given date format.
        /// </summary>
        /// <param name="dateFormat">The date format.</param>
        /// <param name="dateText">The date string value.</param>
        /// <param name="timeText">The Test time string value.</param>
        /// <returns>A nullable <see cref="DateTime"/> instance with date and time.</returns>
        private static DateTime? GetDate(string dateFormat, string dateText, string timeText)
        {
            if (!DateTime.TryParseExact(dateText, dateFormat, null, DateTimeStyles.None, out DateTime date))
            {
                if (dateFormat == UsDateFormat)
                {
                    dateFormat = UkDateFormat;
                }
                else if (dateFormat == UkDateFormat)
                {
                    dateFormat = UsDateFormat;
                }

                if (!DateTime.TryParseExact(dateText, dateFormat, null, DateTimeStyles.None, out date))
                {
                    return null;
                }
            }

            if (!string.IsNullOrEmpty(timeText))
            {
                TimeSpan? time = GetTime(timeText);
                if (time.HasValue)
                {
                    date = date.Add(time.Value);
                }
            }

            return date;
        }

        /// <summary>
        /// Gives the <see cref="TimeSpan"/> of the given time string value based on a data format.
        /// </summary>
        /// <param name="timeText">The time string value.</param>
        /// <returns>A nullable <see cref="TimeSpan"/> instance.</returns>
        private static TimeSpan? GetTime(string timeText)
        {
            if (DateTime.TryParseExact(timeText, TimeFormats, null, DateTimeStyles.None, out DateTime time))
            {
                return time.TimeOfDay;
            }

            return null;
        }

        /// <summary>
        /// Gives the date format based on the date itself or time.
        /// </summary>
        /// <remarks>
        /// First the date it self will be parsed to get a correct date format. if that fails, the test time will
        /// be used to find the date format.
        /// </remarks>
        /// <param name="birthdayText">The date of birth string value.</param>
        /// <param name="reportDateText">The test date string value.</param>
        /// <param name="reportTimeText">The test time.</param>
        /// <returns>The date format in <see cref="UsDateFormat"/> or <see cref="UkDateFormat"/></returns>
        private static string GetDateFormat(string birthdayText, string reportDateText, string reportTimeText)
        {
            string dateFormat = null;
            dateFormat = GetDateFormatFromDate(birthdayText);
            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = GetDateFormatFromDate(reportDateText);
            }

            if (string.IsNullOrEmpty(dateFormat))
            {
                dateFormat = GetDateFormatFromTime(reportTimeText);
            }

            return dateFormat;
        }

        /// <summary>
        /// Gets the date format from the date.
        /// </summary>
        /// <remarks>
        /// finds the date format by dividing the date string in to 3 parts and checks if the first part exceed 12.
        /// If so this date format will be <see cref="UkDateFormat"/>.
        /// If the second part exceeds 12 this date format will be <see cref="UsDateFormat"/>
        /// </remarks>
        /// <param name="date">The date string.</param>
        /// <returns>The date format in <see cref="UkDateFormat"/> or <see cref="UsDateFormat"/></returns>
        private static string GetDateFormatFromDate(string date)
        {
            string[] dateParts = date.Split("-".ToCharArray());
            int firstPart = GetNumber(dateParts[0]);
            int secondPart = GetNumber(dateParts[1]);
            if (firstPart > 12)
            {
                return UkDateFormat;
            }

            if (secondPart > 12)
            {
                return UsDateFormat;
            }

            return null;
        }

        /// <summary>
        /// Gets date format base on time string.
        /// </summary>
        /// <remarks>
        /// If the time contains am or pm, the date format will be <see cref="UsDateFormat"/>
        /// otherwise <see cref="UkDateFormat"/>
        /// </remarks>
        /// <param name="time">The time string.</param>
        /// <returns>The date format in <see cref="UsDateFormat"/> or <see cref="UkTimeFormat"/>.</returns>
        private static string GetDateFormatFromTime(string time)
        {
            if (Regex.IsMatch(time.Trim(), "(am|pm)$", RegexOptions.IgnoreCase))
            {
                return UsDateFormat;
            }

            return UkDateFormat;
        }

        /// <summary>
        /// Parses a string to int.
        /// </summary>
        /// <param name="text">The number string.</param>
        /// <returns>Returns an integer value.</returns>
        private static int GetNumber(string text)
        {
            if (text.StartsWith("0"))
            {
                text = text.Substring(1);
            }

            return int.Parse(text);
        }
    }
}