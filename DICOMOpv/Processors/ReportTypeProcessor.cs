﻿using DICOMOpv.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Processors
{
    public class ReportTypeProcessor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static DeviceType FindDevice(string value)
        {
            if (value.Contains("HFA 3") || value.Contains("HFA ||-i") || value.Contains("HFA Ii 750-1010075") || value.Contains("HFA Ii 750"))
            {
                return DeviceType.HFA3;
            }
            else if (value.Contains("HFA Il") || value.Contains("HFA ll") || value.Contains("HFA 11") || value.Contains("HFA II") || value.Contains("HFA II-i"))
                return DeviceType.HFAIIi;
            else
            {
                log.Error("Could not identify device type.");
                return DeviceType.None;
            }
        }

        /// <summary>
        /// Method that takes unique identifiers defined in template to classify report to a specific language other than english
        /// </summary>
        /// <param name="Text">pdf as text</param>
        /// <param name="report">parameters to identify report type</param>
        /// <returns>Detected Language</returns>
        private static Language IdentifyLanguage(string Text, ReportTemplate report, Language DetectedLanguage)
        {
            if (DetectedLanguage != Language.None)
            {
                return DetectedLanguage;
            }
            else if (report.ES_Identifiers.Any(Text.Contains))
            {
                return Language.ES;
            }
            else
            {
                log.Info("No language keys were identified so default language selected.");
                return Language.EN;
            }
        }

        public static ReportType IdentifyReportType(string value, string DeviceName, Language language = Language.None)
        {
            DeviceType device = DeviceType.None;
            // template for invalid reports
            var reportTemplate = JsonConvert.DeserializeObject<ReportTemplate>(File.ReadAllText(@"./Template/ReportConfig.json"));
            Language detectedLanguage = IdentifyLanguage(value, reportTemplate, language);

            var invalid = new ReportType
            {
                ThresholdType = ThresholdTypes.None,
                DeviceType = device,
                isValid = false,
                DetectedLanguage = detectedLanguage,
                TemplateType = TemplateType.None
            };

            if (string.IsNullOrEmpty(value))
            {
                return invalid;
            }
            else
            {
                //identify device type
                if (string.IsNullOrEmpty(DeviceName))
                {
                    device = FindDevice(value);
                }
                else
                {
                    device = FindDevice(DeviceName);
                }

                if (detectedLanguage == Language.JP)
                {
                    return GetJapaneseReports(value, device, reportTemplate);
                }

                //identify template
                if (reportTemplate.GPA.Any(value.Contains))
                {
                    if (reportTemplate.Central24_2.Any(value.Contains))
                    {
                        return new ReportType
                        {
                            ThresholdType = ThresholdTypes.Central24_2,
                            DeviceType = device,
                            isValid = true,
                            DetectedLanguage = detectedLanguage,
                            TemplateType = TemplateType.T24_2_GPA
                        };
                    }
                    if (reportTemplate.Central30_2.Any(value.Contains))
                    {
                        var template = TemplateType.T30_2_HFAIIi_GPA;
                        if (device == DeviceType.HFA3)
                            template = TemplateType.T30_2_GPA;
                        return new ReportType
                        {
                            ThresholdType = ThresholdTypes.Central30_2,
                            DeviceType = device,
                            isValid = true,
                            DetectedLanguage = detectedLanguage,
                            TemplateType = template
                        };
                    }
                }

                if (reportTemplate.SFA.Any(value.Contains))
                {
                    if (reportTemplate.Central30_2.Any(value.Contains))
                    {
                        var template = TemplateType.T30_2_HFAIIi;
                        if (device == DeviceType.HFA3)
                            template = TemplateType.T30_2;
                        return new ReportType
                        {
                            ThresholdType = ThresholdTypes.Central30_2,
                            DeviceType = device,
                            isValid = true,
                            DetectedLanguage = detectedLanguage,
                            TemplateType = template
                        };
                    }
                    else if (reportTemplate.Central24_2.Any(value.Contains))
                    {
                        var template = TemplateType.T24_2_HFAIIi;
                        if (device == DeviceType.HFA3)
                            template = TemplateType.T24_2; //T24_2
                        return new ReportType
                        {
                            ThresholdType = ThresholdTypes.Central24_2,
                            DeviceType = device,
                            isValid = true,
                            DetectedLanguage = detectedLanguage,
                            TemplateType = template
                        };
                    }
                    else if (reportTemplate.Central10_2.Any(value.Contains))
                    {
                        var template = TemplateType.T10_2_HFAIIi;
                        if (device == DeviceType.HFA3)
                            template = TemplateType.T10_2;
                        return new ReportType
                        {
                            ThresholdType = ThresholdTypes.Central10_2,
                            DeviceType = device,
                            isValid = true,
                            DetectedLanguage = detectedLanguage,
                            TemplateType = template
                        };
                    }
                    else
                        return invalid;
                }
                else
                    return invalid;
            }
        }

        // Currently only supporting HFA3 device outputs for Japanese Reports
        private static ReportType GetJapaneseReports(string value, DeviceType device, ReportTemplate reportTemplate)
        {
            if (reportTemplate.Central30_2_JP.Any(value.Contains))
            {
                var template = TemplateType.T30_2_HFAIIi;
                if (device == DeviceType.HFA3)
                    template = TemplateType.T30_2;
                return new ReportType
                {
                    ThresholdType = ThresholdTypes.Central30_2,
                    DeviceType = device,
                    isValid = true,
                    DetectedLanguage = Language.JP,
                    TemplateType = template
                };
            }
            else if (reportTemplate.Central24_2_JP.Any(value.Contains))
            {
                var template = TemplateType.T24_2_HFAIIi;
                if (device == DeviceType.HFA3)
                    template = TemplateType.T24_2; //T24_2
                return new ReportType
                {
                    ThresholdType = ThresholdTypes.Central24_2,
                    DeviceType = device,
                    isValid = true,
                    DetectedLanguage = Language.JP,
                    TemplateType = template
                };
            }
            else if (reportTemplate.Central10_2_JP.Any(value.Contains))
            {
                var template = TemplateType.T10_2_HFAIIi;
                if (device == DeviceType.HFA3)
                    template = TemplateType.T10_2;
                return new ReportType
                {
                    ThresholdType = ThresholdTypes.Central10_2,
                    DeviceType = device,
                    isValid = true,
                    DetectedLanguage = Language.JP,
                    TemplateType = template
                };
            }
            else // can't identify any valid reports
                return new ReportType
                {
                    ThresholdType = ThresholdTypes.None,
                    DeviceType = device,
                    isValid = false,
                    DetectedLanguage = Language.JP,
                    TemplateType = TemplateType.None
                };
        }
    }
}