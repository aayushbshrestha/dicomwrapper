﻿using DICOMOpv.Util;
using log4net;
using System.Collections.Generic;
using Topcon.DicomWrapper;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Processors
{
    public class Coordinates
    {
        public int Left;
        public int Width;
        public int Top;
        public int Height;

        public Coordinates()
        {
        }

        public Coordinates(int left, int top, int width, int height)
        {
            Left = left;
            Width = width;
            Top = top;
            Height = height;
        }
    }

    public class CoordinateResolver
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Dictionary<string, Coordinates> T24_2_HFAIIi = new Dictionary<string, Coordinates>() {
            { Constants.GAZE, new Coordinates(205, 2873, 2158, 150) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(620,  645 , 720,  820) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1110, 1410, 485,  580) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1105, 2060, 495,  575) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(380,  1410, 485,  580) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(370,  2060, 495,  575) },
            { Constants.GREY_SCALE , new Coordinates(1410, 640, 730, 820) }
        };

        private static readonly Dictionary<string, Coordinates> T24_2 = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(200, 2743, 2158, 150) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(535, 925, 750, 750) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1040, 1630, 505, 500) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1040, 2190, 505, 500) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(275, 1630, 505, 500) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(275, 2190, 505, 500) },
            { Constants.GREY_SCALE , new Coordinates(1380, 925, 750, 750) }
        };

        private static readonly Dictionary<string, Coordinates> T24_2_GPA = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(205, 2743, 2158, 157) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(275, 930, 505, 495) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1040, 1540, 505, 495) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1040, 2195, 505, 495) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(275, 1540, 505, 495) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(275, 2195, 505, 495) },
            { Constants.GREY_SCALE , new Coordinates(1040, 930, 505, 495) }
        };

        private static readonly Dictionary<string, Coordinates> T10_2 = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(200, 2743, 2158, 150) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(535, 925, 750, 750) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1040, 1630, 505, 500) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1040, 2190, 505, 500) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(275, 1630, 505, 500) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(275, 2190, 505, 500) },
            { Constants.GREY_SCALE , new Coordinates(1380, 925, 750, 750) }
        };

        private static readonly Dictionary<string, Coordinates> T10_2_HFAIIi = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(205, 2873, 2158, 150) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(620,  645 , 720,  820) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1110, 1410, 485,  580) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1105, 2060, 495,  575) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(380,  1410, 485,  580) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(370,  2060, 495,  575) },
            { Constants.GREY_SCALE , new Coordinates(1410, 640, 730, 820) }
        };

        private static readonly Dictionary<string, Coordinates> T30_2_HFAIIi = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(205, 2873, 2158, 150) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(620,  645 , 720,  820) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1110, 1410, 485,  580) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1105, 2060, 495,  575) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(380,  1410, 485,  580) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(370,  2060, 495,  575) },
            { Constants.GREY_SCALE , new Coordinates(1410, 640, 730, 820) }
        };

        private static readonly Dictionary<string, Coordinates> T30_2 = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(200, 2743, 2158, 150) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(535, 925, 750, 750) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1040, 1630, 505, 500) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1040, 2190, 505, 500) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(275, 1630, 505, 500) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(275, 2190, 505, 500) },
            { Constants.GREY_SCALE , new Coordinates(1380, 925, 750, 750) }
        };

        private static readonly Dictionary<string, Coordinates> T30_2_GPA = new Dictionary<string, Coordinates>()
        {
            { Constants.GAZE, new Coordinates(205, 2743, 2158, 157) },
            { Constants.SENSITIVITY_VALUE_MATRIX, new Coordinates(275, 930, 505, 495) },
            { Constants.PATTERN_DEV_VALUE_MATRIX, new Coordinates(1040, 1540, 505, 495) },
            { Constants.PATTERN_DEV_SYMBOL_MATRIX , new Coordinates(1040, 2195, 505, 495) },
            { Constants.TOTAL_DEVIATION_VALUE_MATRIX, new Coordinates(275, 1540, 505, 495) },
            { Constants.TOTAL_DEVIATION_SYMBOL_MATRIX , new Coordinates(275, 2195, 505, 495) },
            { Constants.GREY_SCALE , new Coordinates(1040, 930, 505, 495) }
        };

        //TODO : This needs to be filled out with logic for other device types after verfying coordinates for each item
        public static Dictionary<string, Coordinates> GetCoordinates(TemplateType type)
        {
            switch (type)
            {
                case TemplateType.T24_2:
                    return T24_2;

                case TemplateType.T24_2_HFAIIi:
                    return T24_2_HFAIIi;

                case TemplateType.T24_2_GPA:
                    return T24_2_GPA;

                case TemplateType.T10_2:
                    return T10_2;

                case TemplateType.T10_2_HFAIIi:
                    return T10_2_HFAIIi;

                case TemplateType.T30_2:
                    return T30_2;

                case TemplateType.T30_2_GPA:
                    return T30_2_GPA;

                case TemplateType.T30_2_HFAIIi:
                    return T30_2_HFAIIi;
            }

            return null;
        }
    }
}