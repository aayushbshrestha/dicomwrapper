﻿using Dicom;
using log4net;
using System;
using System.Collections.Generic;
using System.Text;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Processors
{
    public class DicomProcessor
    {
        private static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static DicomPersonName GetPersonName(DicomElement element)
        {
            try
            {
                return new DicomPersonName(element.Tag, element.Get<string>(0));
            }
            catch (Exception ex)
            {
                log.Error($"{ex.Message}.Could not generate DicomPersonName for {element}");
                return null;
            }
        }

        public static string GetStringValueForDicomElement(DicomElement element)
        {
            try
            {
                if (element != null)
                    return element.Get<string>();
                else
                    return null;
            }
            catch (Exception ex)
            {
                log.Error($"Could not retrive value for {element.Tag}. Exception : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Helper to check if dicom file is a DICOM OPV
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool CheckModality(DicomElement item)
        {
            try
            {
                if (item != null && item.Get<string>() == "OPV")
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Error($"Could not determine Modality. Ex: {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// Convert Dicom Date string to nullable date time object.
        /// </summary>
        /// <param name="value">yyyymmdd format date string</param>
        /// <returns></returns>
        public static DateTime? DicomDateTimeParser(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    int year = int.Parse(value.Substring(0, 4));
                    int month = int.Parse(value.Substring(4, 2));
                    int day = int.Parse(value.Substring(6, 2));
                    DateTime date = new DateTime(year, month, day);
                    return date;
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Error($"Could not convert to datetime object. {value}, EXCEPTION: {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Overloaded Date time parser
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static DateTime? DicomDateTimeParser(DicomElement item)
        {
            try
            {
                string dateValue = item.Get<string>();
                if (string.IsNullOrEmpty(dateValue))
                {
                    return null;
                }
                string date = dateValue.Substring(0, 8);
                string time = dateValue.Substring(8, 6);
                int year = int.Parse(date.Substring(0, 4));
                int month = int.Parse(date.Substring(4, 2));
                int day = int.Parse(date.Substring(6, 2));
                int hour = int.Parse(time.Substring(0, 2));
                int min = int.Parse(time.Substring(2, 2));
                int sec = int.Parse(time.Substring(4, 2));
                return new DateTime(year, month, day, hour, min, sec);
            }
            catch (Exception ex)
            {
                log.Error($"{ex.Message}, error encountered trying to read Date from Dicom element.");
                return null;
            }
        }

        /// <summary>
        /// Overloaded Date time parser
        /// </summary>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static DateTime? DicomDateTimeParser(string date, string time)
        {
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    int year = int.Parse(date.Substring(0, 4));
                    int month = int.Parse(date.Substring(4, 2));
                    int day = int.Parse(date.Substring(6, 2));
                    int hour = int.Parse(time.Substring(0, 2));
                    int min = int.Parse(time.Substring(2, 2));
                    int sec = int.Parse(time.Substring(4, 2));
                    DateTime _date = new DateTime(year, month, day, hour, min, sec);
                    return _date;
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Error($"Error encountered trying to parse date time date = {date} and time = {time}. Ex message : {ex.Message}");
                return null;
            }
        }

        /// <summary>
        /// Get eye laterality from dicom element object
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Laterality GetLateralityFromDicomItem(DicomElement item)
        {
            try
            {
                string eye = item.Get<string>().ToUpper();
                if (eye == "OD" || eye == "RIGHT" || eye == "R")
                {
                    return Laterality.Right;
                }
                else if (eye == "OS" || eye == "LEFT" || eye == "L")
                {
                    return Laterality.Left;
                }
                else if (eye == "OU" || eye == "RL" || eye == "LR")
                {
                    return Laterality.Both;
                }
                else
                    return Laterality.Unknown;
            }
            catch (Exception ex)
            {
                log.Error($"Error encountered trying to get laterality for dicom item {item}. Exception : {ex.Message}");
                return Laterality.Unknown;
            }
        }

        /// <summary>
        /// Util to parse patient gender from dicom element
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Gender GetPatientGender(DicomElement item)
        {
            try
            {
                string gender = item.Get<string>().ToLower();
                if (string.IsNullOrEmpty(gender))
                    return Gender.Unkown;
                else if (item.Get<string>().ToLower() == "m")
                    return Gender.Male;
                else if (item.Get<string>().ToLower() == "f")
                    return Gender.Female;
                else
                    return Gender.Other;
            }
            catch (Exception ex)
            {
                log.Error($"Could not determine gender. {ex.Message}");
                return Gender.Unkown;
            }
        }
    }
}