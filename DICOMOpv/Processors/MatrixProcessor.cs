﻿using DICOMOpv.Models;
using DICOMOpv.Serializer;
using Topcon.DicomWrapper;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Processors
{
    public class MatrixProcessor
    {
        /// <summary>
        /// Once values are computed construct serializable object for TDVM, PDVM and SVM
        /// </summary>
        public static MatrixItem GetValueMatrixItems(ValueMatrix matrix, ReportType report)
        {
            MatrixItem item = new MatrixItem(matrix.type.ToString(), $"{matrix.type.ToString()}ValueMatrix");
            foreach (var dataItem in matrix.quad1.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            foreach (var dataItem in matrix.quad2.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            foreach (var dataItem in matrix.quad3.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            foreach (var dataItem in matrix.quad4.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            return item;
        }

        /// <summary>
        /// Once images are processed generate list of serializable objects for TDSM and PDSM
        /// </summary>
        public static MatrixItem GetSymbolMatrixItems(SymbolMatrix matrix, ReportType report)
        {
            MatrixItem item = new MatrixItem(matrix.type.ToString(), $"{matrix.type.ToString()}Symbol");
            foreach (var dataItem in matrix.quad1.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            foreach (var dataItem in matrix.quad2.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            foreach (var dataItem in matrix.quad3.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            foreach (var dataItem in matrix.quad4.data)
            {
                item.AddItem(GetDegrees(report.TemplateType, new MatrixItemValue(dataItem.x, dataItem.y, dataItem.value)));
            }
            return item;
        }

        public static MatrixItemValue GetDegrees(TemplateType type, MatrixItemValue iv)
        {
            MatrixItemValue retValue = new MatrixItemValue();
            if (type == TemplateType.T10_2 || type == TemplateType.T10_2_HFAIIi)
            { // degrees are off set of 2
                if (iv.X > 0)
                {
                    retValue.X = 1 + ((iv.X - 1) * 2);
                }
                else
                {
                    if (iv.X < 0)
                    {
                        retValue.X = 1 + (iv.X * 2);
                    }
                }

                if (iv.Y > 0)
                {
                    retValue.Y = 1 + ((iv.Y - 1) * 2);
                }
                else
                {
                    if (iv.Y < 0)
                    {
                        retValue.Y = 1 + (iv.Y * 2);
                    }
                }
            }
            else
            { // T24-2 or T30-2 ; degrees are of set of 6
                if (iv.X > 0)
                {
                    retValue.X = 3 + ((iv.X - 1) * 6);
                }
                else
                {
                    if (iv.X < 0)
                    {
                        retValue.X = 3 + (iv.X * 6);
                    }
                }

                if (iv.Y > 0)
                {
                    retValue.Y = 3 + ((iv.Y - 1) * 6);
                }
                else
                {
                    if (iv.Y < 0)
                    {
                        retValue.Y = 3 + (iv.Y * 6);
                    }
                }
            }
            retValue.Value = iv.Value;
            return retValue;
        }
    }
}