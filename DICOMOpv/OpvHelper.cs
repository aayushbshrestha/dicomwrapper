﻿using Dicom;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Topcon.DicomWrapper
{
    public class OpvHelper
    {
        /// <summary>
        ///   1. create patient demographics
        ///   2. create discrete data nodes eg: FixationMonitor, Fovea, PSD, PupilDiameter, etc
        ///   3. create Matrix elements eg:  TotalDeviationValueMatrix,SensitivityValueMatrix, etc
        ///   4. create PixelData content using EncapsulatePdf();
        ///   5. create metadata common to OPV and/or any missing tags.;
        /// </summary>
        /// <param name="opvObject"></param>
        /// <param name="outFile"></param>
        /// <returns></returns>
        //public static Boolean CreateDicomOpvFile(DicomOpv opvObject, string outFile)
        public static async Task<Boolean> CreateDicomOpvFile(DicomOpv opvObject, string outFile)

        {
            var dicomDataset = new DicomDataset();

            dicomDataset.Add(DicomTag.SoftwareVersions, opvObject.SoftwareVersions);
            dicomDataset.Add(DicomTag.FileMetaInformationGroupLength, opvObject.FileMetaInformationGroupLength.ToString());
            dicomDataset.Add(DicomTag.MediaStorageSOPClassUID, opvObject.MediaStorageSOPClassUID);
            //dicomDataset.Add(DicomTag.MediaStorageSOPInstanceUID, DicomTag.MediaStorageSOPInstanceUID);

            //dicomDataset.Add(DicomTag.TransferSyntaxUID, "1.2.840.10008.1.2.1");
            dicomDataset.Add(DicomTag.TransferSyntaxUID, opvObject.TransferSyntaxUID);

            //dicomDataset.Add(DicomTag.ImplementationClassUID, "1.3.6.1.4.1.30071.8");
            dicomDataset.Add(DicomTag.ImplementationClassUID, opvObject.ImplementationClassUID);

            dicomDataset.Add(DicomTag.ImplementationVersionName, "fo - dicom 4.0.6");
            dicomDataset.Add(DicomTag.SpecificCharacterSet, "ISO_IR 100");
            dicomDataset.Add(DicomTag.InstanceCreationDate, Convert.ToDateTime(opvObject.InstanceCreationDateTime).ToString("yyyyMMdd"));
            dicomDataset.Add(DicomTag.InstanceCreationTime, Convert.ToDateTime(opvObject.InstanceCreationDateTime).ToString("hhmmss"));
            //dicomDataset.Add(DicomTag.InstanceCreationTime, opvObject.InstanceCreationDateTime);
            dicomDataset.Add(DicomTag.SOPClassUID, opvObject.SOPClassUID);
            dicomDataset.Add(DicomTag.SOPInstanceUID, opvObject.SOPInstanceUID);
            dicomDataset.Add(DicomTag.StudyDate, Convert.ToDateTime(opvObject.StudyDateTime).ToString("yyyyMMdd"));
            dicomDataset.Add(DicomTag.ContentDate, Convert.ToDateTime(DateTime.Now).ToString("yyyyMMdd"));
            dicomDataset.Add(DicomTag.StudyInstanceUID, opvObject.StudyInstanceUID);
            dicomDataset.Add(DicomTag.AcquisitionDateTime, Convert.ToDateTime(opvObject.AcquisitionDateTime).ToString("yyyyMMdd"));
            dicomDataset.Add(DicomTag.AccessionNumber, opvObject.AccessionNumber.ToString());
            dicomDataset.Add(DicomTag.Modality, "OPV");
            dicomDataset.Add(DicomTag.ConversionType, opvObject.ConversionType);
            dicomDataset.Add(DicomTag.Manufacturer, opvObject.Manufacturer);
            dicomDataset.Add(DicomTag.InstitutionName, opvObject.InstitutionName);
            dicomDataset.Add(DicomTag.InstitutionAddress, opvObject.InstitutionAddress);
            dicomDataset.Add(DicomTag.ReferringPhysicianName, "ReferringPhysicianName");
            dicomDataset.Add(DicomTag.StationName, opvObject.StationName);
            dicomDataset.Add(DicomTag.StudyDescription, opvObject.StudyDescription);
            //MISSING (0008,1032)	ProcedureCodeSequence	SQ	1	-1
            dicomDataset.Add(DicomTag.InstitutionalDepartmentName, "Ophthalmology_Modifiled");
            dicomDataset.Add(DicomTag.NameOfPhysiciansReadingStudy, "Physican^Demomodified");
            dicomDataset.Add(DicomTag.ManufacturerModelName, opvObject.ManufacturerModelName);
            dicomDataset.Add(DicomTag.PatientName, opvObject.PatientName.ToString());
            dicomDataset.Add(DicomTag.PatientID, opvObject.PatientID);
            dicomDataset.Add(DicomTag.PatientBirthDate, Convert.ToDateTime(opvObject.PatientDOB).ToString("yyyyMMdd"));
            dicomDataset.Add(DicomTag.PatientSex, opvObject.PatientSex);
            dicomDataset.Add(DicomTag.DeviceSerialNumber, opvObject.DeviceSerialNumber);
            //            dicomDataset.Add(DicomTag.SecondaryCaptureDeviceID, opvObject.sec);
            dicomDataset.Add(DicomTag.SecondaryCaptureDeviceManufacturer, "OCULUS Optikgeraete GmbH-Modified");
            dicomDataset.Add(DicomTag.SecondaryCaptureDeviceManufacturerModelName, "Easyfield-modified");
            //dicomDataset.Add(DicomTag.SoftwareVersions, "\\3.19r1477");
            dicomDataset.Add(DicomTag.SeriesInstanceUID, opvObject.SeriesInstanceUID);
            dicomDataset.Add(DicomTag.StudyID, opvObject.StudyID);// "1");
            dicomDataset.Add(DicomTag.SeriesNumber, "1");
            dicomDataset.Add(DicomTag.InstanceNumber, opvObject.InstanceNumber.ToString());
            dicomDataset.Add(DicomTag.Laterality, opvObject.Laterality);
            dicomDataset.Add(DicomTag.ImageLaterality, opvObject.Laterality);
            //dicomDataset.Add(DicomTag.BurnedInAnnotation, "NO");
            dicomDataset.Add(DicomTag.DocumentTitle, opvObject.DocumentTitle);
            dicomDataset.Add(DicomTag.ProcedureCodeSequence, opvObject.ProcedureCodeSequence.Items[0]);
            dicomDataset.Add(DicomTag.PerformedProcedureStepStartDate, opvObject.PerformedProcedureStepStartDate);
            dicomDataset.Add(DicomTag.PerformedProcedureStepStartTime, opvObject.PerformedProcedureStepStartTime);
            dicomDataset.Add(opvObject.FixationSequence);
            dicomDataset.Add(opvObject.VisualFieldCatchTrialSequence);
            dicomDataset.Add(DicomTag.VisualFieldTestDuration, opvObject.VisualFieldTestDuration);
            dicomDataset.Add(opvObject.BackgroundIlluminationColorCodeSequence);
            dicomDataset.Add(DicomTag.BackgroundLuminance, opvObject.BackgroundLuminance);
            dicomDataset.Add(opvObject.OphthalmicPatientClinicalInformationRightEyeSequence);
            dicomDataset.Add(opvObject.ResultsNormalsSequence);
            dicomDataset.Add(DicomTag.FovealSensitivity, opvObject.FovealSensitivity);
            /* End non-matrix discrete elements (Appx 18 nodes)  */

            /* Start matrix elements    */
            dicomDataset.Add(opvObject.VisualFieldTestPointSequence);
            /* Start matrix elements    */

            //to embedd PDF document inside dicom opv
            byte[] pdf = opvObject.EncapsulatedDocument.Buffer.Data;
            dicomDataset.Add(DicomTag.VerificationFlag, "VERIFIED");
            dicomDataset.Add(DicomTag.MIMETypeOfEncapsulatedDocument, "application/pdf");
            dicomDataset.Add(DicomTag.EncapsulatedDocument, pdf);

            // to save the final output into disk
            DicomFile newDicom = new DicomFile(dicomDataset);
            await newDicom.SaveAsync(outFile);

            //newDicom.Save(outFile);

            return true;
        }

        //ProcCodeSequence
        public string ProcCodeSequence_CodeValue { get; set; }

        public string ProcCodeSequence_CodingSchemeDesignator { get; set; }
        public string ProcCodeSequence_CodeMeaning { get; set; }

        //TestDuration
        public float TestDuration_VisualFieldTestDuration { get; set; }

        //BackgroundLuminance
        public float BackgroundLuminance_Background { get; set; }

        //OphthalmicPatientClinicalInformationRightEyeSequence
        public string OphthalmicPatientClinicalInformationRightEyeSequence_PupilSize { get; set; }

        public string OphthalmicPatientClinicalInformationRightEyeSequence_SphericalLensPower { get; set; }
        public string OphthalmicPatientClinicalInformationRightEyeSequence_CylinderLensPower { get; set; }
        public string OphthalmicPatientClinicalInformationRightEyeSequence_CylinderAxis { get; set; }

        //TestDate
        public string TestDate_PerformedProcedureStepStartDate { get; set; }

        //TestTime
        public string TestTime_PerformedProcedureStepStarTime { get; set; }

        // FixationSequence
        public string FixationSequence_CodeMeaning { get; set; }

        public string FixationSequence_FixationCheckedQuantity { get; set; }
        public string FixationSequence_PatientNotProperlyFixatedQuantity { get; set; }
        public string FixationSequence_ExcessiveFixationLosses { get; set; }

        //VisualFieldCatchTrialSequence
        public string VisualFieldCatchTrialSequence_FalsePositivesQuantity { get; set; }

        public string VisualFieldCatchTrialSequence_PositiveCatchTrialsQuantity { get; set; }
        public string VisualFieldCatchTrialSequence_ExcessiveFalsePositivesDataFlag { get; set; }

        public string VisualFieldCatchTrialSequence_FalseNegativesQuantity { get; set; }
        public string VisualFieldCatchTrialSequenceNegativeCatchTrialsQuantity { get; set; }
        public string VisualFieldCatchTrialSequence_ExcessiveFalseNegativesDataFlag { get; set; }

        //ResultsNormalsSequence
        public string ResultsNormalsSequence_GlobalDeviationFromNormalg { get; set; }

        public string ResultsNormalsSequence_LocalizedDeviationFromNormal { get; set; }

        //PerformedProtocolCodeSequence
        public string PerformedProtocolCodeSequence_CodeMeaning { get; set; }

        //BackgroundIlluminationColorCodeSequence
        public string BackgroundIlluminationColorCodeSequence_CodeMeaning { get; set; }

        //Fovea
        public float Fovea_FovealSensitivity { get; set; }

        //collection of VisualFieldTestPoints (5 matrix node values)
        public List<VisualFieldTestPoint> VFTPoints { get; set; }

        /// <summary>
        /// TODO : This is where all values need to be set, have to come from matrix extractor.
        /// </summary>
        public void PopulateDiscreteNodesAndVisualTestPointSequence()
        {
            ProcCodeSequence_CodeValue = "Easyfiled_NEW";       //TODO :value  have to come from matrix extractor.
            ProcCodeSequence_CodingSchemeDesignator = "EASY"; //TODO :value  have to come from matrix extractor.
            ProcCodeSequence_CodeMeaning = "Visual Field Measurement..";//TODO :value  have to come from matrix extractor.

            TestDuration_VisualFieldTestDuration = 208;

            BackgroundLuminance_Background = 10;

            OphthalmicPatientClinicalInformationRightEyeSequence_PupilSize = "4.4";
            OphthalmicPatientClinicalInformationRightEyeSequence_SphericalLensPower = "4";
            OphthalmicPatientClinicalInformationRightEyeSequence_CylinderLensPower = "1.5";
            OphthalmicPatientClinicalInformationRightEyeSequence_CylinderAxis = "56";

            TestDate_PerformedProcedureStepStartDate = "20201111";

            TestTime_PerformedProcedureStepStarTime = "070907.0705";

            FixationSequence_CodeMeaning = "Macular Fixation Testing";
            FixationSequence_FixationCheckedQuantity = "2";
            FixationSequence_PatientNotProperlyFixatedQuantity = "0";
            FixationSequence_ExcessiveFixationLosses = "NO";

            VisualFieldCatchTrialSequence_FalsePositivesQuantity = "0";
            VisualFieldCatchTrialSequence_PositiveCatchTrialsQuantity = "8";
            VisualFieldCatchTrialSequence_ExcessiveFalsePositivesDataFlag = "NO";

            VisualFieldCatchTrialSequence_FalseNegativesQuantity = "0";
            VisualFieldCatchTrialSequenceNegativeCatchTrialsQuantity = "14";
            VisualFieldCatchTrialSequence_ExcessiveFalseNegativesDataFlag = "NO";

            ResultsNormalsSequence_GlobalDeviationFromNormalg = "-18.32";
            ResultsNormalsSequence_LocalizedDeviationFromNormal = "10.27";

            PerformedProtocolCodeSequence_CodeMeaning = "Visual Field Full Threshold Test Strategy";

            BackgroundIlluminationColorCodeSequence_CodeMeaning = "White"; ;

            Fovea_FovealSensitivity = 31;

            VFTPoints = PopulateVisualFieldTestPointSequence();
        }

        #region "Discete data nodes"

        public DicomSequence ProcCodeSequence()
        {
            DicomDataset sqContent = new DicomDataset();
            sqContent.Add(DicomTag.CodeValue, ProcCodeSequence_CodeValue);
            sqContent.Add(DicomTag.CodingSchemeDesignator, ProcCodeSequence_CodingSchemeDesignator);
            sqContent.Add(DicomTag.CodeMeaning, ProcCodeSequence_CodeMeaning);
            DicomSequence sq = new DicomSequence(DicomTag.ProcedureCodeSequence, sqContent);
            return sq;
        }

        public float GetTestDuration()
        {
            float visualFieldTestDuration = TestDuration_VisualFieldTestDuration;
            return visualFieldTestDuration;
        }

        public float GetBackgroundLuminance()
        {
            float backgroundLuminance = BackgroundLuminance_Background;
            return backgroundLuminance;
        }

        /// <summary>
        /// sets the values for 3 elements, PupilDiameter, VisualAcuity & Rx in csi exam xml object
        /// TODO - may need the same for left eye as well ??
        /// </summary>
        /// <returns>DicomSequence</returns>

        public DicomSequence GetOphthalmicPatientClinicalInformationRightEyeSequence()
        {
            DicomDataset sqContent = new DicomDataset();
            DicomDataset sqContentChild = new DicomDataset();

            //PupilSize(PupilDiameter)
            sqContentChild.Add(DicomTag.PupilSize, OphthalmicPatientClinicalInformationRightEyeSequence_PupilSize);
            sqContent.Add(sqContentChild);

            //VisualAcuity(VisualAcuity)          //TODO - not able to find
            //sqContentChild.Add(DicomTag.VisualAcuityMeasurementSequence, 4.4);
            //DicomTag.DecimalVisualAcuity

            //Rx  (Spherical Lens Power, Cylinder Lens Power, Cylinder Axis)
            DicomDataset sqInnerChild = new DicomDataset();
            sqInnerChild.Add(DicomTag.SphericalLensPower, OphthalmicPatientClinicalInformationRightEyeSequence_SphericalLensPower);
            sqInnerChild.Add(DicomTag.CylinderLensPower, OphthalmicPatientClinicalInformationRightEyeSequence_CylinderLensPower);
            sqInnerChild.Add(DicomTag.CylinderAxis, OphthalmicPatientClinicalInformationRightEyeSequence_CylinderAxis);
            DicomSequence sqCInnerChild = new DicomSequence(DicomTag.RefractiveParametersUsedOnPatientSequence, sqInnerChild);
            sqContent.Add(sqCInnerChild);

            DicomSequence sqParent = new DicomSequence(DicomTag.OphthalmicPatientClinicalInformationRightEyeSequence, sqContent);
            return sqParent;
        }

        //public DicomDate GetTestDate()
        public string GetTestDate()
        {
            return TestDate_PerformedProcedureStepStartDate;
        }

        public string GetTestTime()
        {
            //string performedProcedureStepStarTime = "070907.0705"; // Convert.ToDateTime("070907.0705").ToString("hhmmss.ffffff");
            return TestTime_PerformedProcedureStepStarTime;
        }

        /// <summary>
        /// FixationSequeunce
        /// </summary>
        /// <returns>DicomSequence containing FixationMonitor &  FixationLosses </returns>
        public DicomSequence GetFixationSequence()
        {
            DicomDataset sqContent = new DicomDataset();

            //add first item ( child)
            DicomDataset sqContentChild = new DicomDataset();
            sqContentChild.Add(DicomTag.CodeMeaning, FixationSequence_CodeMeaning);
            DicomSequence sqChild = new DicomSequence(DicomTag.FixationMonitoringCodeSequence, sqContentChild);
            sqContent.Add(sqChild);
            sqContent.Add(DicomTag.FixationCheckedQuantity, FixationSequence_FixationCheckedQuantity);
            sqContent.Add(DicomTag.PatientNotProperlyFixatedQuantity, FixationSequence_PatientNotProperlyFixatedQuantity);
            sqContent.Add(DicomTag.ExcessiveFixationLosses, FixationSequence_ExcessiveFixationLosses);
            DicomSequence sqParent = new DicomSequence(DicomTag.FixationSequence, sqContent);
            return sqParent;
        }

        /// <summary>
        /// FalsePosErrors & FalseNegErrors
        /// </summary>
        /// <returns>DicomSequence</returns>
        public DicomSequence GetVisualFieldCatchTrialSequence()
        {
            DicomDataset sqContent = new DicomDataset();
            DicomDataset sqContentChild = new DicomDataset();

            //FalsePosErrors
            sqContentChild.Add(DicomTag.FalsePositivesQuantity, VisualFieldCatchTrialSequence_FalsePositivesQuantity);
            sqContentChild.Add(DicomTag.PositiveCatchTrialsQuantity, VisualFieldCatchTrialSequence_PositiveCatchTrialsQuantity);
            sqContentChild.Add(DicomTag.ExcessiveFalsePositivesDataFlag, VisualFieldCatchTrialSequence_ExcessiveFalsePositivesDataFlag);

            //FalseNegErrors
            sqContentChild.Add(DicomTag.FalseNegativesQuantity, VisualFieldCatchTrialSequence_FalseNegativesQuantity);
            sqContentChild.Add(DicomTag.NegativeCatchTrialsQuantity, VisualFieldCatchTrialSequenceNegativeCatchTrialsQuantity);
            sqContentChild.Add(DicomTag.ExcessiveFalseNegativesDataFlag, VisualFieldCatchTrialSequence_ExcessiveFalseNegativesDataFlag);

            sqContent.Add(sqContentChild);
            DicomSequence sqParent = new DicomSequence(DicomTag.VisualFieldCatchTrialSequence, sqContent);
            return sqParent;
        }

        /// <summary>
        /// sets values for nodes, MD and PSD
        /// </summary>
        /// <returns>DicomSequence</returns>
        public DicomSequence GetResultsNormalsSequence()
        {
            DicomDataset sqContent = new DicomDataset();
            sqContent.Add(DicomTag.GlobalDeviationFromNormal, ResultsNormalsSequence_GlobalDeviationFromNormalg);     //MD
            sqContent.Add(DicomTag.LocalizedDeviationFromNormal, ResultsNormalsSequence_LocalizedDeviationFromNormal);      //PSD
            DicomSequence sq = new DicomSequence(DicomTag.ResultsNormalsSequence, sqContent);
            return sq;
        }

        /// <summary>
        /// Strategy:Visual Field Full Threshold Test Strategy
        /// </summary>
        /// <returns>DicomSequence</returns>
        public DicomSequence GetPerformedProtocolCodeSequence()
        {
            DicomDataset sqContent = new DicomDataset();
            sqContent.Add(DicomTag.CodeMeaning, PerformedProtocolCodeSequence_CodeMeaning);
            DicomSequence sq = new DicomSequence(DicomTag.PerformedProtocolCodeSequence, sqContent);
            return sq;
        }

        /// <summary>
        /// StimulusArea
        /// </summary>
        /// <returns>DicomSequence</returns>
        public DicomSequence GetBackgroundIlluminationColorCodeSequence()
        {
            DicomDataset sqContent = new DicomDataset();
            DicomDataset sqContentChild = new DicomDataset();
            sqContentChild.Add(DicomTag.CodeMeaning, BackgroundIlluminationColorCodeSequence_CodeMeaning);
            DicomSequence sq = new DicomSequence(DicomTag.BackgroundIlluminationColorCodeSequence, sqContent);
            return sq;
        }

        public float GetFovea()
        {
            return Fovea_FovealSensitivity;
        }

        #endregion "Discete data nodes"

        #region "VisualFieldTestPointSequence"

        public short TestPointsCount { get; set; } //TODO this has to come from matrix extractor, sample values can be 54 or 72

        public class VisualFieldTestPointNormalsSequence
        {
            public float AgeCorrectedSensitivityDeviationValue { get; set; }
            public string GeneralizedDefectCorrectedSensitivityDeviationFlag { get; set; }
            public float GeneralizedDefectCorrectedSensitivityDeviationValue { get; set; }
            public float GeneralizedDefectCorrectedSensitivityDeviationProbabilityValue { get; set; }
        }

        public class VisualFieldTestPoint
        {
            public float VisualFieldTestPointXCoordinate { get; set; }
            public float VisualFieldTestPointYCoordinate { get; set; }
            public string StimulusResults { get; set; }
            public float SensitivityValue { get; set; }
            public VisualFieldTestPointNormalsSequence VFTestPointNormalsSequence { get; set; }
        }

        public List<VisualFieldTestPoint> PopulateVisualFieldTestPointSequence()
        {
            List<VisualFieldTestPoint> _VFTestPoints = null;
            _VFTestPoints = new List<VisualFieldTestPoint>();

            //set first item
            VisualFieldTestPoint visualFieldTestPoint = new VisualFieldTestPoint
            {
                VisualFieldTestPointXCoordinate = -27,  //TODO Replace value from matrix extractor.
                VisualFieldTestPointYCoordinate = 9,
                StimulusResults = "SEEN", //TODO Replace value from matrix extractor.
                SensitivityValue = 0, //TODO Replace value from matrix extractor.

                VFTestPointNormalsSequence = new VisualFieldTestPointNormalsSequence
                {
                    AgeCorrectedSensitivityDeviationValue = -26, //TODO Replace value from matrix extractor.
                    //sqContentChild.Add(DicomTag.Unknown, .5); //tag not found ? //TODO
                    GeneralizedDefectCorrectedSensitivityDeviationFlag = "YES", //TODO Replace value from matrix extractor.
                    GeneralizedDefectCorrectedSensitivityDeviationValue = -19, //TODO Replace value from matrix extractor.
                    GeneralizedDefectCorrectedSensitivityDeviationProbabilityValue = float.Parse(".5", System.Globalization.CultureInfo.InvariantCulture) //TODO Replace value from matrix extractor.
                },
            };
            _VFTestPoints.Add(visualFieldTestPoint);

            //set second item
            visualFieldTestPoint = new VisualFieldTestPoint
            {
                VisualFieldTestPointXCoordinate = -27,  //TODO Replace value from matrix extractor.
                VisualFieldTestPointYCoordinate = 3,
                StimulusResults = "SEEN", //TODO Replace value from matrix extractor.
                SensitivityValue = 0, //TODO Replace value from matrix extractor.

                VFTestPointNormalsSequence = new VisualFieldTestPointNormalsSequence
                {
                    AgeCorrectedSensitivityDeviationValue = -27, //TODO Replace value from matrix extractor.
                    //sqContentChild.Add(DicomTag.Unknown, .5); //tag not found ? //TODO
                    GeneralizedDefectCorrectedSensitivityDeviationFlag = "YES", //TODO Replace value from matrix extractor.
                    GeneralizedDefectCorrectedSensitivityDeviationValue = -19, //TODO Replace value from matrix extractor.
                    GeneralizedDefectCorrectedSensitivityDeviationProbabilityValue = float.Parse(".5", System.Globalization.CultureInfo.InvariantCulture) //TODO Replace value from matrix extractor.
                },
            };
            _VFTestPoints.Add(visualFieldTestPoint);

            return _VFTestPoints;
        }

        /// <summary>
        /// GetVisualFieldTestPointSequence.Get 5 matrix node values for SensitivityValue,TotalDeviationValueMatrix,PatternDeviationValueMatrix,TotalDeviationSymbolMatrix and         PatternDeviationSymbolMatrix
        /// </summary>
        /// <returns>DicomSequence for all test points </returns>
        public DicomSequence GetVisualFieldTestPointSequence(List<VisualFieldTestPoint> VisualFieldTestPoints)
        {
            if (VisualFieldTestPoints == null || VisualFieldTestPoints.Count == 0)
                return null;

            int totalTestPoints = VisualFieldTestPoints.Count;
            // convert the values into dicom sequence
            DicomDataset[] sqContentArray = new DicomDataset[totalTestPoints];

            for (int i = 0; i < totalTestPoints; i++)
            {
                DicomDataset sqContent = new DicomDataset();
                sqContent.Add(DicomTag.VisualFieldTestPointXCoordinate, VisualFieldTestPoints[i].VisualFieldTestPointXCoordinate);
                //sqContent.Add(DicomTag.Unknown, .5); //tag not found ? //TODO
                sqContent.Add(DicomTag.VisualFieldTestPointYCoordinate, VisualFieldTestPoints[i].VisualFieldTestPointYCoordinate);
                sqContent.Add(DicomTag.StimulusResults, VisualFieldTestPoints[i].StimulusResults);
                sqContent.Add(DicomTag.SensitivityValue, VisualFieldTestPoints[i].SensitivityValue);

                //add  child - matrix element
                DicomDataset sqContentChild = new DicomDataset();
                sqContentChild.Add(DicomTag.AgeCorrectedSensitivityDeviationValue, VisualFieldTestPoints[i].VFTestPointNormalsSequence.AgeCorrectedSensitivityDeviationValue);
                sqContentChild.Add(DicomTag.GeneralizedDefectCorrectedSensitivityDeviationFlag, VisualFieldTestPoints[i].VFTestPointNormalsSequence.GeneralizedDefectCorrectedSensitivityDeviationFlag);
                sqContentChild.Add(DicomTag.GeneralizedDefectCorrectedSensitivityDeviationValue, VisualFieldTestPoints[i].VFTestPointNormalsSequence.GeneralizedDefectCorrectedSensitivityDeviationValue);
                sqContentChild.Add(DicomTag.GeneralizedDefectCorrectedSensitivityDeviationProbabilityValue, VisualFieldTestPoints[i].VFTestPointNormalsSequence.GeneralizedDefectCorrectedSensitivityDeviationProbabilityValue);

                DicomSequence sqChild = new DicomSequence(DicomTag.VisualFieldTestPointNormalsSequence, sqContentChild);
                sqContent.Add(sqChild);
                sqContentArray[i] = sqContent;
            }
            DicomSequence sqParent = new DicomSequence(DicomTag.VisualFieldTestPointSequence, sqContentArray);
            return sqParent;
        }

        #endregion "VisualFieldTestPointSequence"
    }
}