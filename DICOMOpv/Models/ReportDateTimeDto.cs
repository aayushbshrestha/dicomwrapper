﻿using System;

namespace DICOMOpv.Models
{
    public class ReportDateTimeDto
    {
        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        /// Gets or sets the report date time.
        /// </summary>
        public DateTime ReportDateTime { get; set; }
    }
}