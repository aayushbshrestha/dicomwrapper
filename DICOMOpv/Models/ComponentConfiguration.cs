﻿using System.Collections.Generic;

namespace DICOMOpv.Models
{
    public class ComponentConfiguration
    {
        public class BaseTemplate
        {
            public string MachineType { get; set; }

            // list that holds all configs for text objects
            public IList<TextConfiguration> TextComponentConfigurations { get; } = new List<TextConfiguration>();
        }

        public class TextConfiguration : DicomTextConfiguration
        {
            public string Key { get; set; }
            public IList<string> EmbedKeys { get; set; }
            public string LookupRegx { get; set; }
            public IList<string> NextKeys { get; set; }
            public string SerializationKey { get; set; }
            public string OriginalKey { get; set; }
            public string Corrector { get; set; }
            public string Purifier { get; set; }
            public string ProjectionPattern { get; set; }
            public List<string> ProjectionElements { get; set; }

            /// <summary>
            /// Gets the DICOM text configuration.
            /// </summary>
            public DicomTextConfiguration DICOM { get; } = new DicomTextConfiguration(); // TODO
        }

        public class DicomTextConfiguration
        {
            /// <summary>
            /// Gets or sets the format of value representation.
            /// </summary>
            public string Formatter { get; set; }

            /// <summary>
            /// Gets or sets the delimiter of value representation.
            /// </summary>
            public string Delimiter { get; set; }

            /// <summary>
            /// Gets or sets the list of DICOM tag of the element.
            /// </summary>
            public IList<DiTag> Tags { get; set; }
        }

        /// <summary>
        /// Represents a set of components of a DICOM tag.
        /// </summary>
        public class DiTag
        {
            /// <summary>
            /// Gets or sets a value indicating whether the tag is required.
            /// </summary>
            public bool IsRequired { get; set; } = true;

            /// <summary>
            /// Gets or sets the data type of the element to be extracted.
            /// </summary>
            public string DataType { get; set; }

            /// <summary>
            /// Gets or sets the group ID of the element to be extracted.
            /// </summary>
            public string Group { get; set; }

            /// <summary>
            /// Gets or sets the element ID of the element to be extracted.
            /// </summary>
            public string Element { get; set; }

            /// <summary>
            /// Gets or sets the creator ID of the element to be extracted.
            /// </summary>
            public string Creater { get; set; }

            /// <summary>
            /// Gets or sets the format of the element to be represented.
            /// </summary>
            public string Formatter { get; set; }
        }
    }
}