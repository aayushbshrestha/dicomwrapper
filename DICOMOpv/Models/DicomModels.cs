﻿namespace DICOMOpv.Models
{
    /// <summary>
    /// Data structure for DICOM Sequence type
    /// </summary>
    public class SequenceItem
    {
        public string CodeValue { get; set; }   //SH - short string
        public string CodingSchemeGenerator { get; set; }   //SH - short string
        public string CodeMeaning { get; set; } //LO - long string
    }

    /// <summary>
    /// Data structure for DICOM Discretedata type in the csi output xml
    /// </summary>
    public class DiscreteDefinition
    {
        public string UID { get; set; }
        public string DiscreteGroupID { get; set; }
        public string DiscreteGroupName { get; set; }
        public string DiscreteDimensionID { get; set; }
        public string DiscreteDimensionName { get; set; }
        public string MeasurementUnitID { get; set; }
        public string MeasurementUnitName { get; set; }
        public string MeasurementUnitShortName { get; set; }
        public string DataValue { get; set; }
        public string DataExtType { get; set; }
    }

    /// <summary>
    /// Data structure for DICOM Discrete data item type
    /// </summary>
    public class DiscreteDataItem
    {
        public string DiscreteFieldName { get; set; }
        public string DataValue { get; set; }
        public string DataExtType { get; set; }
    }
}