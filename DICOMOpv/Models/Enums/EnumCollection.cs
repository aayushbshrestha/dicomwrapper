﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DICOMOpv.Models.Enums
{
    public class EnumCollection
    {
        public enum Gender { Male, Female, Other, Unkown }

        public enum Laterality { Left, Right, Both, Unknown }

        public enum ThresholdTypes { None, Central10_2, Central24_2, Central30_2 }

        public enum DeviceType { None, HFAIIi, HFA2, HFA3 }

        public enum FileType { PDF, DICOM, JPG, Unkown }

        public enum Language { None, EN, ES, JP } // needs to be updated with more supported languages as samples become available.

        public enum TemplateType
        {
            /// Unknown.
            None,

            /// 10-2 HFA3.
            T10_2,

            /// 10-2 HFA2.
            T10_2_HFAIIi,

            /// 24-2 HFA3.
            T24_2,

            /// 10-2 HFA2.
            T24_2_HFAIIi,

            /// 30-2 HFA3.
            T30_2,

            T30_2_GPA,

            /// 30-2 HFA2.
            T30_2_HFAIIi,

            T30_2_HFAIIi_GPA,

            /// 24-2 HFA3_GPA.
            T24_2_GPA
        }
    }
}