﻿using DICOMOpv.Util;
using System;
using System.Collections.Generic;

namespace DICOMOpv.Models
{
    public enum MatrixTypes { TotalDeviation, PatternDeviation, Sensitivity }

    public class ValueMatrix
    {
        public Quadrant1 quad1;
        public Quadrant2 quad2;
        public Quadrant3 quad3;
        public Quadrant4 quad4;
        public MatrixTypes type;

        public void SetType(string value)
        {
            if (value == Constants.TOTAL_DEVIATION_VALUE_MATRIX) type = MatrixTypes.TotalDeviation;
            else if (value == Constants.PATTERN_DEV_VALUE_MATRIX) type = MatrixTypes.PatternDeviation;
            else if (value == Constants.SENSITIVITY_VALUE_MATRIX)
            {
                type = MatrixTypes.Sensitivity;
                // can be limited to quad 1 , 2 row 5 and row1 for quad 3,4
                //Sanitize(); Todo: Verify if this is necessary
            }
        }

        //this step is required for hfa2 sensitivity matrix values since they seem to contain impurities because of the axis dividers.
        private void Sanitize()
        {
            foreach (var data in quad1.data)
            {
                if (data.value.Contains("-"))
                {
                    data.value = data.value.Replace("-", "");
                }
            }

            foreach (var data in quad2.data)
            {
                if (data.value.Contains("-"))
                {
                    data.value = data.value.Replace("-", "");
                }
            }

            foreach (var data in quad3.data)
            {
                if (data.value.Contains("-"))
                {
                    data.value = data.value.Replace("-", "");
                }
            }

            foreach (var data in quad4.data)
            {
                if (data.value.Contains("-"))
                {
                    data.value = data.value.Replace("-", "");
                }
            }
        }
    }

    public class SymbolMatrix
    {
        public Quadrant1 quad1;
        public Quadrant2 quad2;
        public Quadrant3 quad3;
        public Quadrant4 quad4;
        public MatrixTypes type;

        public void SetType(string value)
        {
            if (value == Constants.TOTAL_DEVIATION_VALUE_MATRIX || value == Constants.TOTAL_DEVIATION_SYMBOL_MATRIX) type = MatrixTypes.TotalDeviation;
            else if (value == Constants.PATTERN_DEV_VALUE_MATRIX || value == Constants.PATTERN_DEV_SYMBOL_MATRIX) type = MatrixTypes.PatternDeviation;
            else if (value == Constants.SENSITIVITY_VALUE_MATRIX) type = MatrixTypes.Sensitivity;
        }
    }

    /// <summary>
    /// Quadrant Data will hold the data read from OCR along with the position of the data
    /// All the data will be loaded left to right so when translating the position into coordinates
    /// the quadrant will need to be accounted for as well as the laterality of the eye.
    /// </summary>
    public class QuadrantData
    {
        public string value;
        public int row;
        public int col;

        //coordinates for degree transformation
        public int x { get; set; }

        public int y { get; set; }
    }

    public abstract class Quadrant
    {
        public abstract void ConvertCoordinates();

        public abstract void PrintCoordinates();
    }

    /// <summary>
    /// Upper left quadrant
    /// </summary>
    public class Quadrant1 : Quadrant
    {
        public Quadrant1(List<QuadrantData> _data)
        {
            data = _data;
        }

        public List<QuadrantData> data;

        public override void ConvertCoordinates()
        {
            foreach (var point in data)
            {
                GetCoordinates(point);
            }
        }

        private void GetCoordinates(QuadrantData _data)
        {
            if (_data != null)
            {
                _data.x = -5 + _data.col;
                _data.y = 5 - _data.row;
            }
        }

        public override void PrintCoordinates()
        {
            foreach (var point in data)
            {
                Console.WriteLine($"(row = {point.row} , col = {point.col})< ---- >(x = {point.x} y = {point.y} )");
            }
        }
    }

    /// <summary>
    /// Upper right quadrant
    /// </summary>
    public class Quadrant2 : Quadrant
    {
        public Quadrant2(List<QuadrantData> _data)
        {
            data = _data;
        }

        public List<QuadrantData> data;

        public override void ConvertCoordinates()
        {
            foreach (var point in data)
            {
                GetCoordinates(point);
            }
        }

        private void GetCoordinates(QuadrantData _data)
        {
            if (_data != null)
            {
                _data.x = _data.col + 1;
                _data.y = 5 - _data.row;
            }
        }

        public override void PrintCoordinates()
        {
            foreach (var point in data)
            {
                Console.WriteLine($"(row = {point.row} , col = {point.col})< ---- >(x = {point.x} y = {point.y} )");
            }
        }
    }

    /// <summary>
    /// Lower left quadrant
    /// </summary>
    public class Quadrant3 : Quadrant
    {
        public Quadrant3(List<QuadrantData> _data)
        {
            data = _data;
        }

        public List<QuadrantData> data;

        public override void ConvertCoordinates()
        {
            foreach (var point in data)
            {
                GetCoordinates(point);
            }
        }

        private void GetCoordinates(QuadrantData _data)
        {
            if (_data != null)
            {
                _data.x = -5 + _data.col;
                _data.y = -1 - _data.row;
            }
        }

        public override void PrintCoordinates()
        {
            foreach (var point in data)
            {
                Console.WriteLine($"(row = {point.row} , col = {point.col})< ---- >(x = {point.x} y = {point.y} )");
            }
        }
    }

    /// <summary>
    /// Lower right quadrant.
    /// </summary>
    public class Quadrant4 : Quadrant
    {
        public Quadrant4(List<QuadrantData> _data)
        {
            data = _data;
        }

        public List<QuadrantData> data;

        public override void ConvertCoordinates()
        {
            foreach (var point in data)
            {
                GetCoordinates(point);
            }
        }

        private void GetCoordinates(QuadrantData _data)
        {
            if (_data != null)
            {
                _data.x = _data.col + 1;
                _data.y = -1 - _data.row;
            }
        }

        public override void PrintCoordinates()
        {
            foreach (var point in data)
            {
                Console.WriteLine($"(row = {point.row} , col = {point.col})< ---- >(x = {point.x} y = {point.y} )");
            }
        }
    }
}