﻿namespace DICOMOpv.Models
{
    public class ReportTemplate
    {
        public string[] SFA;
        public string[] GPA;
        public string[] Central10_2;
        public string[] Central10_2_JP;
        public string[] Central24_2;
        public string[] Central24_2_JP;
        public string[] Central30_2;
        public string[] Central30_2_JP;
        public string[] ES_Identifiers;
    }
}