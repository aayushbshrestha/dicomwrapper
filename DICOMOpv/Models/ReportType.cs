﻿using System;
using System.Collections.Generic;
using System.Text;
using static DICOMOpv.Models.Enums.EnumCollection;

namespace DICOMOpv.Models
{
    /// <summary>
    ///This struct contains all the information required to determine what sort of Report we're dealing with
    /// </summary>
    public class ReportType
    {
        public ThresholdTypes ThresholdType;
        public TemplateType TemplateType;
        public DeviceType DeviceType;
        public Language DetectedLanguage;
        public bool isValid;
    }
}