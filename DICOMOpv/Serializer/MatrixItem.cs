﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DICOMOpv.Serializer
{
    public class MatrixItem : IXmlSerializable
    {
        public string SerializationKey;

        /// <summary>
        /// Gets or sets the name of the key tag displayed in the XML.
        /// </summary>
        [XmlAttribute("field")]
        public string Name { get; set; }

        /// <summary>
        /// Gets the list of values of the value tag displayed in the XML.
        /// </summary>
        [XmlElement(ElementName = "element", Namespace = "M")]
        public List<MatrixItemValue> Values { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatrixItem"/> class.
        /// </summary>
        public MatrixItem()
        {
            Values = new List<MatrixItemValue>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatrixItem"/> class.
        /// </summary>
        /// <param name="name">The XML key tag.</param>
        /// <param name="serializationKey">The key used for XML serialization.</param>
        public MatrixItem(string name, string serializationKey)
        {
            Name = name;
            SerializationKey = serializationKey;
            Values = new List<MatrixItemValue>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatrixItem"/> class.
        /// </summary>
        /// <param name="name">Name of the XML key.</param>
        /// <param name="serializationKey">The key used for XML serialization.</param>
        /// <param name="values">The coordinates and its values of the matix.</param>
        public MatrixItem(string name, string serializationKey, List<MatrixItemValue> values)
        {
            Name = name;
            SerializationKey = serializationKey;
            Values = values;
        }

        /// <summary>
        /// Add a matrix item to the list of values.
        /// </summary>
        /// <param name="matrixItem">Instance of the <see cref="MatrixItemValue"/> class.</param>
        public void AddItem(MatrixItemValue matrixItem)
        {
            Values.Add(matrixItem);
        }

        /// <summary>
        /// Adds the Matrix Items to the List of Values
        /// </summary>
        /// <param name="matrixItems">Enum of the Matrix Items</param>
        public void AddRange(IEnumerable<MatrixItemValue> matrixItems)
        {
            Values.AddRange(matrixItems);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("matrix");
            writer.WriteAttributeString("field", string.IsNullOrWhiteSpace(SerializationKey) ? Name : SerializationKey);

            foreach (MatrixItemValue val in Values)
            {
                val.WriteXml(writer);
            }

            writer.WriteEndElement();
        }
    }

    public class MatrixItemValue : IXmlSerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MatrixItemValue"/> class.
        /// </summary>
        public MatrixItemValue()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MatrixItemValue"/> class.
        /// </summary>
        /// <param name="x">X cordinate.</param>
        /// <param name="y">Y cordinate.</param>
        /// <param name="value">Value of the matrix item.</param>
        public MatrixItemValue(int x, int y, string value)
        {
            X = x;
            Y = y;
            Value = value;
        }

        /// <summary>
        /// Gets or sets the x-axis of the matrix item.
        /// </summary>
        [XmlAttribute("x")]
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the y-axis of the matrix item.
        /// </summary>
        [XmlAttribute("y")]
        public int Y { get; set; }

        /// <summary>
        /// Gets or sets the Value tag of the matrix item.
        /// </summary>
        [XmlElement("Value")]
        public string Value { get; set; }

        /// <summary>
        /// This method is reserved and should not be used. When implementing the IXmlSerializable interface,
        /// you should return null from this method, and instead, if specifying a custom schema is required,
        /// apply the XmlSchemaProviderAttribute to the class.
        /// </summary>
        /// <returns>An XmlSchema that describes the XML representation of the object that is produced by the WriteXml <see cref="XmlWriter"/>
        /// method and consumed by the ReadXml <see cref="XmlWriter"/> method.</returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">The XmlReader stream from which the object is deserialized.</param>
        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The customized serialization format.
        /// </summary>
        /// <param name="writer">Instance of the <see cref="XmlWriter"/> class.</param>
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("element");
            writer.WriteAttributeString("x", $"{X}");
            writer.WriteAttributeString("y", $"{Y}");

            writer.WriteStartElement("Value");
            writer.WriteValue(Value);
            writer.WriteFullEndElement();

            writer.WriteEndElement();
        }
    }
}