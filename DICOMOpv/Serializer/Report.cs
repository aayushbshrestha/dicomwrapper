﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DICOMOpv.Serializer
{
    public class Report : IXmlSerializable
    {
        /// <summary>
        /// Gets or sets the device type. This can be HFA-2 or HFA-3.
        /// </summary>
        [XmlElement(ElementName = "element", Namespace = "device")]
        public MachineTypeItem MachineType { get; set; }

        /// <summary>
        /// Gets or sets source file type of the report.
        /// </summary>
        [XmlElement(ElementName = "element", Namespace = "file")]
        public ReportSource FileType { get; set; }

        /// <summary>
        /// Gets or sets the list of <see cref="TextItem"/> class of the report.
        /// </summary>
        [XmlElement(ElementName = "element", Namespace = "")]
        public List<TextItem> TextItems { get; set; }

        [XmlElement(ElementName = "element", Namespace = "")]
        public List<MatrixItem> MatrixItems { get; set; }

        [XmlElement(ElementName = "element", Namespace = "")]
        public List<ImageItem> ImageItems { get; set; }

        public Report()
        {
            TextItems = new List<TextItem>();
        }

        public Report(
                    MachineTypeItem machineType,
                    ReportSource fileType,
                    List<TextItem> textItem,
                    List<MatrixItem> matrixItems,
                    List<ImageItem> imageItems
            //List<ImageTextItem> imageTextItems
            )
        {
            MachineType = machineType;
            FileType = fileType;
            TextItems = textItem;
            MatrixItems = matrixItems;
            ImageItems = imageItems;
            //ImageTextItems = imageTextItems;
        }

        /// <summary>
        /// Adds the <see cref="TextItem"/> to the list of text items.
        /// </summary>
        /// <param name="textItem">Instance of the <see cref="TextItem"/> to be added.</param>
        public void AddTextItem(TextItem textItem)
        {
            TextItems.Add(textItem);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            MachineType.WriteXml(writer);
            FileType.WriteXml(writer);
            foreach (TextItem textItem in TextItems)
            {
                textItem.WriteXml(writer);
            }
            foreach (MatrixItem matrix in MatrixItems)
            {
                matrix.WriteXml(writer);
            }
            foreach (ImageItem image in ImageItems)
            {
                image.WriteXml(writer);
            }
        }
    }
}