﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DICOMOpv.Serializer
{
    public class TextItem : IXmlSerializable
    {
        /// <summary>
        /// Gets or sets the name of the Key tag displayed in the XML.
        /// </summary>
        [XmlAttribute("field")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets dictionary of tags and their values of the XML key tag.
        /// </summary>
        [XmlIgnore]
        public Dictionary<string, string> Values { get; set; }

        /// <summary>
        /// Gets or sets serialization key value of a particular tag.
        /// </summary>
        [XmlIgnore]
        public string SerializationKey { get; set; }

        [XmlIgnore]
        public string OriginalKey { get; set; }

        public TextItem()
        {
        }

        public TextItem(string name, Dictionary<string, string> values, string key)
        {
            Name = name;
            Values = values;
            SerializationKey = key;
        }

        public void SetOriginalKey(string key)
        {
            OriginalKey = key;
        }

        /// <summary>
        /// Gets or sets the value tag of the XML key.
        /// </summary>
        public virtual string Value
        {
            get => Values.ContainsKey("Value") ? Values["Value"] : null;

            set
            {
                if (Values.ContainsKey("Value"))
                {
                    Values["Value"] = value;
                }
            }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("element");
            writer.WriteAttributeString("field", string.IsNullOrWhiteSpace(SerializationKey) ? Name : SerializationKey);
            if (!string.IsNullOrEmpty(OriginalKey))
            {
                writer.WriteAttributeString("OriginalLabel", OriginalKey);
            }

            foreach (KeyValuePair<string, string> value in Values)
            {
                WriteElement(writer, value);
            }
            writer.WriteEndElement();
        }

        private void WriteElement(XmlWriter writer, KeyValuePair<string, string> value)
        {
            writer.WriteStartElement(value.Key);
            writer.WriteValue(value.Value);
            writer.WriteFullEndElement();
        }
    }
}