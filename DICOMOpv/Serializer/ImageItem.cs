﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DICOMOpv.Serializer
{
    public class ImageItem : IXmlSerializable
    {
        private string SerializationKey;

        /// <summary>
        /// Gets or sets the name of the key tag displayed in the XML.
        /// </summary>
        [XmlAttribute("field")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the image byte value of the XML key.
        /// </summary>
        [XmlElement("image")]
        public string Value { get; set; }

        public ImageItem()
        {
        }

        public ImageItem(string name, string value, string serializationKey)
        {
            Name = name;
            Value = value;
            SerializationKey = serializationKey;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("element");
            writer.WriteAttributeString("field", string.IsNullOrWhiteSpace(SerializationKey) ? Name : SerializationKey);
            writer.WriteElementString("image", Value);
            writer.WriteFullEndElement();
        }
    }
}