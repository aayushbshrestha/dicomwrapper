﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DICOMOpv.Serializer
{
    public class ReportSource : IXmlSerializable
    {
        public ReportSource()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSource"/> class.
        /// </summary>
        /// <param name="reportSourceType">Report file type.</param>
        public ReportSource(string reportSourceType)
        {
            ReportSourceType = reportSourceType;
        }

        /// <summary>
        /// Gets or sets the name of the key tag displayed in the XML.
        /// </summary>
        [XmlAttribute("field")]
        public string Field { get; set; } = "FileType";

        /// <summary>
        /// Gets or sets the name of the value tag displayed in the XML.
        /// </summary>
        [XmlElement("Value")]
        public string ReportSourceType { get; set; }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("element");
            writer.WriteAttributeString("field", Field);
            writer.WriteElementString("Value", ReportSourceType);
            writer.WriteEndElement();
        }
    }
}