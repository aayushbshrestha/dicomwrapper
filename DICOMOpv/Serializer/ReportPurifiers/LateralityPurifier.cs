﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class LateralityPurifier : IPurifier
    {
        /// <summary>
        /// Purifies the laterality value tag with OS or OD value.
        /// </summary>
        /// <param name="report">Extracted <see cref="Report"/> class with the XML tags.</param>
        /// <param name="key">Name of the false error key as named in the configuration json file.</param>
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            if (string.IsNullOrEmpty(key))
                key = "Laterality";
            TextItem laterlity = GetLaterlity(textItems);
            // check for when laterality is unknown
            if (laterlity == null)
            {
                laterlity = new TextItem("Laterality", new Dictionary<string, string>()
                {
                    { "Value", "Unknown" },
                }, "Laterality");
            }
            laterlity.SerializationKey = key;
            SetLaterlity(laterlity);
        }

        /// <summary>
        /// Gets the laterality key from the extracted XML tags.
        /// </summary>
        /// <param name="report">Instance of the <see cref="Report"/> class.</param>
        /// <returns>Instance of the <see cref="TextItem"/> class that matches the laterality key.</returns>
        private TextItem GetLaterlity(IList<TextItem> report)
        {
            TextItem laterlity = report.FirstOrDefault(t => t.Name == "Laterality");

            if (laterlity == null)
            {
                laterlity = report.FirstOrDefault(t => t.Name == "Eye");
            }

            return laterlity;
        }

        /// <summary>
        /// Sets the laterality value to a OS or OD.
        /// Left is OS and Right is OD.
        /// </summary>
        /// <param name="textItem">Instance of the <see cref="TextItem"/> class which has the unit tag value to be changed.</param>
        private void SetLaterlity(TextItem textItem)
        {
            textItem.Value = Convert(textItem.Value);
        }

        /// <summary>
        /// Converts laterality.
        /// </summary>
        /// <param name="lateralityText">The text value.</param>
        /// <returns>
        /// The laterality value.
        /// left = OS
        /// right = OD
        /// </returns>
        public static string Convert(string lateralityText)
        {
            string convertedValue = lateralityText;
            if (!string.IsNullOrWhiteSpace(lateralityText))
            {
                switch (lateralityText.ToUpper())
                {
                    case "L":
                    case "LEFT":
                    case "OS":
                        convertedValue = "OS";
                        break;

                    case "R":
                    case "RIGHT":
                    case "OD":
                        convertedValue = "OD";
                        break;

                    default:
                        convertedValue = string.Empty;
                        break;
                }
            }

            return convertedValue;
        }
    }
}