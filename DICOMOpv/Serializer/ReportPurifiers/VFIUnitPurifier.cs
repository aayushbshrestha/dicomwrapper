﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class VFIUnitPurifier : IPurifier
    {
        /// <summary>
        /// Percentage symbol
        /// </summary>
        private const string Percentage = "%";

        /// <summary>
        /// Zero point.
        /// </summary>
        private const string ZeroPoint = "0.";

        /// <summary>
        /// Unit keyword.
        /// </summary>
        private const string Unit = "Unit";

        /// <summary>
        /// Purifies the element by adding percentage symbol to the unit tag.
        /// </summary>
        /// <param name="report">Instance of the <see cref="Report"/> class.</param>
        /// <param name="key">Name of the FalseError key as named in the configuration json file.</param>
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            if (!string.IsNullOrEmpty(key))
            {
                TextItem vfi = textItems.FirstOrDefault(x => x.Name == key);

                if (vfi.Values.ContainsKey(Unit))
                {
                    bool percentageIsDecimal = false;
                    if (vfi.Value.StartsWith(ZeroPoint))
                    {
                        percentageIsDecimal = true;
                    }

                    SetUnitTag(vfi, percentageIsDecimal);
                }
            }
        }

        /// <summary>
        /// Sets the unit tag with percentage if it doesn't have a divider or starts with zero point.
        /// </summary>
        /// <param name="textItem">Instance of the <see cref="TextItem"/> class which has the unit tag value to be changed.</param>
        /// <param name="percentageIsDecimal">If value tag starts with zero point in the value tag.</param>
        private void SetUnitTag(TextItem textItem, bool percentageIsDecimal)
        {
            if (textItem.Values.TryGetValue(Unit, out string unitTag) && unitTag != null && !unitTag.Contains(Percentage))
            {
                textItem.Values[Unit] = percentageIsDecimal ? string.Empty : Percentage;
            }
            else
            {
                textItem.Values[Unit] = string.IsNullOrWhiteSpace(textItem.Values[Unit]) ? (percentageIsDecimal ? string.Empty : Percentage) : textItem.Values[Unit];
            }
        }
    }
}