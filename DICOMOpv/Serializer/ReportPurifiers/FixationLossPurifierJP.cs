﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class FixationLossPurifierJP : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            TextItem fixationLossValue = textItems.FirstOrDefault(t => t.Name == key);
            fixationLossValue.Value = ConstructCorrectValue(fixationLossValue.Value);
        }

        private string ConstructCorrectValue(string key)
        {
            StringBuilder sb = new StringBuilder();
            if (key.Length == 4)
            {
                for (int i = 0; i < key.Length; i++)
                {
                    if (i == 1)
                    {
                        sb.Append("/");
                    }
                    else
                        sb.Append(key[i]);
                }
                return sb.ToString();
            }
            return key;
        }
    }
}