﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    internal class FixationMonitorPurifier : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            if (!string.IsNullOrEmpty(key))
            {
                TextItem fixationMonitor = textItems.FirstOrDefault(x => x.Name == key);
                SanitizeExtraValues(fixationMonitor);
            }
        }

        private void SanitizeExtraValues(TextItem item)
        {
            //if extra value gets read split and take the original value only
            if (item.Value.Contains("Estimulo:"))
            {
                var stringVal = item.Value.Split("Estimulo:");
                item.Value = stringVal[0];
            }
        }
    }
}