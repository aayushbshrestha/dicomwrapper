﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    internal class VisualAcuityPurifier : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            TextItem visAcuity = textItems.FirstOrDefault(x => x.Name == key);
            if (visAcuity != null)
            {
                PurifyValue(visAcuity);
            }
        }

        private void PurifyValue(TextItem value)
        {
            if (!value.Value.Any(char.IsDigit))
            {
                value.Value = "";
            }
        }
    }
}