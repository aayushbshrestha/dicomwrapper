﻿using DICOMOpv.Models;
using DICOMOpv.Processors;
using DICOMOpv.Serializer.ReportPurifiers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DICOMOpv.Serializer
{
    public class ReportDatePurifier : IPurifier
    {
        private const string REPORT_DATE_FORMAT = "MMM dd, yyyy";
        private const string REPORT_TIME_FORMAT = "H:mm";
        private const string REPORT_DATE_TIME_FORMAT = "MMM dd, yyyy H:mm";

        public void Purify(IList<TextItem> Items, string key = null)
        {
            TextItem dateOfBirth = Items.FirstOrDefault(t => t.Name == "DOB" || t.Name == "DateOfBirth");
            TextItem reportDate = Items.FirstOrDefault(t => t.Name == "TestDate");
            TextItem reportTime = Items.FirstOrDefault(t => t.Name == "TestTime");
            bool shouldPurify = true;

            if (dateOfBirth != null && reportDate != null && reportTime != null)
            {
                DateTime dob = DateTime.Now;
                string time = reportTime.Value;

                if (DateTime.TryParseExact(dateOfBirth.Value, REPORT_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dob))
                {
                    shouldPurify = false;
                }
                if (DateTime.TryParseExact(reportDate.Value, REPORT_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dob)
                    && DateTime.TryParseExact(reportTime.Value, REPORT_TIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dob))
                {
                    reportDate.Value = $"{reportDate.Value} {reportTime.Value}";
                }
                else if (DateTime.TryParseExact(reportDate.Value, REPORT_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dob))
                {
                    time = DateTime.Parse(reportTime.Value).ToString(REPORT_TIME_FORMAT);
                    reportDate.Value = $"{reportDate.Value} {time}";
                }

                Items.Remove(reportTime);
            }
            else
            {
                shouldPurify = false;
            }

            if (shouldPurify)
            {
                ReportDateTimeDto reportDateTimeDto = DateTimeConverter.Convert(dateOfBirth.Value, reportDate.Value, reportTime.Value);
                dateOfBirth.Value = reportDateTimeDto.Birthday.ToString(REPORT_DATE_FORMAT);
                reportDate.Value = reportDateTimeDto.ReportDateTime.ToString(REPORT_DATE_TIME_FORMAT);
            }
        }
    }
}