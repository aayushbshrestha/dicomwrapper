﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    internal class PupilDiameterPurifier : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            TextItem pupilDiameter = textItems.FirstOrDefault(x => x.Name == key);
            if (pupilDiameter != null)
            {
                PurifyValue(pupilDiameter);
            }
        }

        //remove accidentally read values
        private void PurifyValue(TextItem value)
        {
            if (!value.Value.Any(char.IsDigit))
            {
                value.Value = "";
            }
        }
    }
}