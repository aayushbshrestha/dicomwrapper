﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class GenderPurifier : IPurifier
    {
        /// <summary>
        /// Purifies the gender value tag with M or F value.
        /// </summary>
        /// <param name="report">Instance of the <see cref="Report"/> class.</param>
        /// <param name="key">Name of the false error key as named in the configuration json file.</param>
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            if (string.IsNullOrEmpty(key)) { key = "Gender"; }
            TextItem gender = textItems.FirstOrDefault(t => t.Name == key);

            if (gender != null)
            {
                gender.Value = Convert(gender.Value);
            }
        }

        /// <summary>
        /// Convert gender value to M, F or O.
        /// </summary>
        /// <param name="genderText">The gender value.</param>
        /// <returns>
        /// The converted value
        /// Male = M
        /// Female = F
        /// Other = O
        /// </returns>
        private static string Convert(string genderText)
        {
            string convertedValue = genderText ?? string.Empty;
            if (!string.IsNullOrWhiteSpace(genderText))
            {
                switch (genderText.ToLower())
                {
                    case "male":
                    case "m":
                        convertedValue = "M";
                        break;

                    case "female":
                    case "f":
                        convertedValue = "F";
                        break;

                    case "other":
                    case "o":
                        convertedValue = "O";
                        break;

                    default:
                        convertedValue = genderText;
                        break;
                }
            }

            return convertedValue;
        }
    }
}