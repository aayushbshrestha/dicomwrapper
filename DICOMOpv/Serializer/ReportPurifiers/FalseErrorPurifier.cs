﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class FalseErrorPurifier : IPurifier
    {
        /// <summary>
        /// Percentage symbol
        /// </summary>
        private const string Percentage = "%";

        /// <summary>
        /// Zero point.
        /// </summary>
        private const string ZeroPoint = "0.";

        /// <summary>
        /// Divider symbol.
        /// </summary>
        private const string Divider = "/";

        /// <summary>
        /// Unit keyword.
        /// </summary>
        private const string Unit = "Unit";

        /// <summary>
        /// Purifies the element by adding percentage symbol to the unit tag.
        /// </summary>
        /// <param name="report">Instance of the <see cref="Report"/> class.</param>
        /// <param name="key">Name of the FalseError key as named in the configuration json file.</param>
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            if (!string.IsNullOrEmpty(key))
            {
                TextItem falseError = textItems.FirstOrDefault(x => x.Name == key);

                if (falseError.Values.ContainsKey(Unit))
                {
                    bool hasDividerOrZeroPoint = false;
                    if (falseError.Value.Contains(Divider))
                    {
                        hasDividerOrZeroPoint = true;
                    }
                    else if (falseError.Value.StartsWith(ZeroPoint))
                    {
                        hasDividerOrZeroPoint = true;
                    }

                    SetUnitTag(falseError, hasDividerOrZeroPoint);
                }
            }
        }

        /// <summary>
        /// Sets the unit tag with percentage if it doesn't have a divider or starts with zero point.
        /// </summary>
        /// <param name="textItem">Instance of the <see cref="TextItem"/> class which has the unit tag value to be changed.</param>
        /// <param name="hasDividerOrZeroPoint">If value tag have a  divider or value tag starts zero point.</param>
        private void SetUnitTag(TextItem textItem, bool hasDividerOrZeroPoint)
        {
            if (textItem.Values.TryGetValue(Unit, out string unitTag) && unitTag != null && !unitTag.Contains(Percentage))
            {
                textItem.Values[Unit] = hasDividerOrZeroPoint ? string.Empty : Percentage;
            }
            else
            {
                textItem.Values[Unit] = string.IsNullOrWhiteSpace(textItem.Values[Unit]) ? (hasDividerOrZeroPoint ? string.Empty : Percentage) : textItem.Values[Unit];
            }
        }
    }
}