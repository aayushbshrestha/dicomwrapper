﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    internal class ThresholdTypePurifier : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            TextItem fixationLossValue = textItems.FirstOrDefault(t => t.Name == key);
            fixationLossValue.Value = ConstructCorrectValue(fixationLossValue.Value);
        }

        private string ConstructCorrectValue(string value)
        {
            return $"Central {value} Threshold Test";
        }
    }
}