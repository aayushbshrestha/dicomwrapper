﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class StimulusPurifier : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            if (string.IsNullOrEmpty(key)) { key = "Stimulus"; }
            TextItem stimulus = textItems.FirstOrDefault(t => t.Name == key);

            if (stimulus != null)
            {
                stimulus.Value = Convert(stimulus.Value);
            }
        }

        private string Convert(string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;
            else if (value.Contains("lll"))
                value = value.Replace("lll", "III");
            else if (value.Contains("Ill"))
                value = value.Replace("Ill", "III");
            else if (value.Contains("|||"))
                value = value.Replace("|||", "III");
            else if (value.Contains("川"))
                value = value.Replace("川", "III");

            return value;
        }
    }
}