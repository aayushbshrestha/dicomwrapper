﻿namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class PurifierFactory
    {
        public static IPurifier Get(string dataPurifier)
        {
            IPurifier purifier = null;
            switch (dataPurifier)
            {
                case "Laterality":
                    purifier = new LateralityPurifier();
                    break;

                case "Gender":
                    purifier = new GenderPurifier();
                    break;

                case "PupilDiameter":
                    purifier = new PupilDiameterPurifier();
                    break;

                case "ReportDates":
                    purifier = new ReportDatePurifier();
                    break;

                case "JPFixationLoss":
                    purifier = new FixationLossPurifierJP();
                    break;

                case "JPReportDates":
                    purifier = new DateTimePurifierJP();
                    break;

                case "ThresholdType":
                    purifier = new ThresholdTypePurifier();
                    break;

                case "VFIUnit":
                    purifier = new VFIUnitPurifier();
                    break;

                case "FalseError":
                    purifier = new FalseErrorPurifier();
                    break;

                case "dbUnit":
                    purifier = new DecibelUnitPurifier();
                    break;

                case "Stimulus":
                    purifier = new StimulusPurifier();
                    break;

                case "FixationMonitor":
                    purifier = new FixationMonitorPurifier();
                    break;

                case "VisualAcuity":
                    purifier = new VisualAcuityPurifier();
                    break;
            }

            return purifier;
        }
    }
}