﻿using System.Collections.Generic;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public interface IPurifier
    {
        void Purify(IList<TextItem> textItems, string key = null);
    }
}