﻿using System.Collections.Generic;
using System.Linq;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class DecibelUnitPurifier : IPurifier
    {
        private const string Desibel = "dB";
        private const string Unit = "Unit";

        /// <summary>
        /// Purifies the element by adding dB to the unit tag of the identified key if the unit is empty.
        /// </summary>
        /// <param name="report">Instance of the <see cref="Report"/> class.</param>
        /// <param name="key">Name of the key tag.</param>
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            TextItem config = textItems.FirstOrDefault(x => x.Name == key);

            if (!string.IsNullOrEmpty(key) && config != null)
            {
                if (config.Values.ContainsKey(Unit))
                {
                    SetUnitTag(config);
                }
            }
        }

        /// <summary>
        /// Sets the unit tag to decibels if its null or contains different unit.
        /// </summary>
        /// <param name="textItem">Instance of the <see cref="TextItem"/> class which has the unit tag value to be changed.</param>
        private void SetUnitTag(TextItem textItem)
        {
            if (textItem.Values.TryGetValue(Unit, out string unitTag) && unitTag != null && !unitTag.Contains(Desibel))
            {
                textItem.Values[Unit] = Desibel;
            }
            else
            {
                textItem.Values[Unit] = string.IsNullOrWhiteSpace(textItem.Values[Unit]) ? Desibel : textItem.Values[Unit];
            }
        }
    }
}