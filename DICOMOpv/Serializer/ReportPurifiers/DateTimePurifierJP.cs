﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICOMOpv.Serializer.ReportPurifiers
{
    public class DateTimePurifierJP : IPurifier
    {
        public void Purify(IList<TextItem> textItems, string key = null)
        {
            TextItem reportDate = textItems.FirstOrDefault(t => t.Name == key);
            reportDate.Value = ConstructCorrectDate(reportDate.Value);
        }

        public string ConstructCorrectDate(string key)
        {
            StringBuilder sb = new StringBuilder();
            if (key.Length == 10)
            {
                for (int i = 0; i < key.Length; i++)
                {
                    if (i == 4 || i == 7)
                    {
                        sb.Append("/");
                    }
                    else
                        sb.Append(key[i]);
                }
                return sb.ToString();
            }
            return key;
        }
    }
}