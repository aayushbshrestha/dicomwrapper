﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DICOMOpv.Serializer
{
    public class MachineTypeItem : IXmlSerializable
    {
        public MachineTypeItem()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MachineTypeItem"/> class.
        /// </summary>
        /// <param name="machineType">The machine type.</param>
        public MachineTypeItem(string machineType)
        {
            MachineType = machineType;
        }

        /// <summary>
        /// Gets or sets the name of the key tag displayed in the XML.
        /// </summary>
        [XmlAttribute("field")]
        public string Field { get; set; } = "MachineType";

        /// <summary>
        /// Gets or sets the name of the value tag displayed in the XML.
        /// </summary>
        [XmlElement("Value")]
        public string MachineType { get; set; }

        /// <summary>
        /// This method is reserved and should not be used. When implementing the IXmlSerializable interface,
        /// you should return null from this method, and instead, if specifying a custom schema is required,
        /// apply the XmlSchemaProviderAttribute to the class.
        /// </summary>
        /// <returns>An XmlSchema that describes the XML representation of the object that is produced by the WriteXml <see cref="XmlWriter"/>
        /// method and consumed by the ReadXml <see cref="XmlWriter"/> method.</returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The customized serialization format.
        /// </summary>
        /// <param name="writer">Instance of the <see cref="XmlWriter"/> class.</param>
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("element");
            writer.WriteAttributeString("field", Field);
            writer.WriteElementString("Value", MachineType);
            writer.WriteFullEndElement();
        }

        XmlSchema IXmlSerializable.GetSchema()
        {
            throw new NotImplementedException();
        }
    }
}