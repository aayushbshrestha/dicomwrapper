﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Topcon.DicomWrapper;

namespace Tester
{
    internal class Program
    {
        private static readonly string PathToDicomFiles = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources");

        private static async Task Main(string[] args)
        {
            try
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                DicomOpv dicomObj = new DicomOpv(@"C:\Users\ashrestha\Documents\Code\Dicom\dicomwrapper\Tester\Japanese\24_2_1.pdf", "JP");
                dicomObj.Extract();
                //timer.Restart();
                dicomObj.ExportReportXML();
                //timer.Stop();
                //Console.WriteLine($"Time to export = {timer.Elapsed}");

                Console.Read();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception encountered {ex.Message}");
            }
        }
    }
}