﻿using System;
using System.IO;
using Topcon.DicomWrapper;

namespace DicomOpvRun
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DicomOpv dicomObj = new DicomOpv(args[0]);
                dicomObj.Extract();
                string outputFile = dicomObj.ExportReportXML(args[1]);
                File.Move(outputFile, args[0] + ".xml");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception encountered {ex.Message}");
            }
        }
    }
}
